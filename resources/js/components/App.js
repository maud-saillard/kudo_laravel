import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import Header from "./layouts/Header";
import Contact from "./pages/Contact";
import Footer from "./layouts/Footer";
import ScrollToTop from "./components/ScrollToTop";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import ResetPassword from "./pages/ResetPassword";
import NewPassword from "./pages/NewPassword";

function App() {
    const [authenticated, setAuthenticated] = useState(false);

    useEffect( () => {
        if(localStorage.getItem('token')) {
            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
            setAuthenticated(true);
        }
    }, [authenticated]);

    return (
        <Router>
            <Header/>
            <ScrollToTop/>
            <Switch>
                <Route path="/reset-password" component={ResetPassword}/>
                <Route path="/reset/:token/:email" component={NewPassword}/>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Login}/>
                <Route path="/" component={Home}/>
            </Switch>
            <Contact/>
            <Footer/>
        </Router>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
