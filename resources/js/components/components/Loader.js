import React from 'react';

function Loader() {
    return (
        <div className="loader">
            <div className="spinner-border green" role="status">
                <span className="sr-only">Chargement...</span>
            </div>
        </div>
    )
}

export default Loader;
