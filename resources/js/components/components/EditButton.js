import React, {useState} from 'react';
import Animate from "./Animate";

function EditButton({edit, destroy}) {
    const [showDropdown, setShowDropdown] = useState(false);
    const toggleDropdown = () => { setShowDropdown(!showDropdown); };

    return (
        <div className="dropdown-edit">
            <div className={'edit-button ' + (showDropdown ? 'opened' : '')} onClick={toggleDropdown}>
                <i className="fas fa-pen"></i>
            </div>

            <Animate className={"dropdown-edit-card"} show={showDropdown}>
                <div className="h-100 w-100 d-flex justify-content-center align-items-center flex-column">
                    <div className="link" onClick={() => {setShowDropdown(false); destroy();}}>Supprimer</div>
                    <div className="link" onClick={() => {setShowDropdown(false); edit();}}>Renommer</div>
                </div>
            </Animate>
        </div>
    )
}

export default EditButton;
