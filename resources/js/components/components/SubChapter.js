import React from 'react';

function Chapter({subChapter}) {
    return (
        <div className="chapter-card">
            <div className="vignette"></div>
            <div className="w-100 d-flex justify-content-between align-items-center py-3 pr-3">
                <div className="title-chapter">{subChapter.name}</div>
                <div className="nb-chapters">{ subChapter.cards.length } carte{ subChapter.cards.length > 1 && 's' }</div>
            </div>
        </div>
    );
}

export default Chapter;
