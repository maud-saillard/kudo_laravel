import React from 'react';
import ReactDOM from 'react-dom';
import {Editor, EditorState, RichUtils} from 'draft-js';
import 'draft-js/dist/Draft.css';

function MyEditor() {
    const [editorState, setEditorState] = React.useState(EditorState.createEmpty());

    const editor = React.useRef(null);

    function focusEditor() {
        editor.current.focus();
    }

    React.useEffect(() => {
        focusEditor()
    }, []);

    const boldText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'BOLD');
        setEditorState(nextState);
    };

    const italicText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'ITALIC');
        setEditorState(nextState);
    };

    const underlineText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');
        setEditorState(nextState);
    };

    const test = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'HIGHLIGHT');
        setEditorState(nextState);
    };

    return (
        <div>
            <button onMouseDown = { (e) => boldText(e)}>Bold</button>
            <button onMouseDown = { (e) => italicText(e)}>Italic</button>
            <button onMouseDown = { (e) => underlineText(e)}>Underline</button>
            <button onMouseDown = { (e) => test(e)}>Test</button>
            <div>
                <Editor
                    ref={editor}
                    editorState={editorState}
                    onChange={editorState => setEditorState(editorState)}
                    customStyleMap={styleMap}
                />
            </div>
        </div>
    );
}

const styleMap = {
    'HIGHLIGHT': {
        'backgroundColor': '#faed27',
    }
};

export default MyEditor;
