import React, {useState, useEffect} from 'react';

function Animate(props) {
    const [show, setShow] = useState(props.show);
    const [animation, setAnimation] = useState(true);

    useEffect( () => {
        if(props.show && animation) {
            setShow(props.show);
            animate();
        }

        if(!props.show && !animation) {
            setAnimation(true);
            setTimeout(function(){ setShow(props.show);}, 100);
        }
    }, [props.show]);

    function animate() {
        return new Promise(resolve => setTimeout(function(){ setAnimation(!animation); resolve(); }, 100));
    }

    return (
        <div className={props.className + (animation ? ' fade' : '')} style={{ display: show ? 'block': 'none'}}>
            { props.children }
        </div>
    );
}

export default Animate;
