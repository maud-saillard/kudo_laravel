import React from 'react';

function Chapter({chapter}) {
    return (
        <div className="chapter-card">
            <div className="vignette"></div>
            <div className="w-100 d-flex justify-content-between align-items-center py-3 pr-3">
                <div className="title-chapter">{chapter.name}</div>
                <div className="nb-chapters">{ chapter.subChapters.length } sous chapitre{ chapter.subChapters.length > 1 ? 's' : '' }</div>
            </div>
        </div>
    );
}

export default Chapter;
