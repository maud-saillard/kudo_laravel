import React from 'react';

function Classroom({classroom}) {
    return (
        <div className="subject-card">
            <h3>{ classroom.name }</h3>
            <div>{ classroom.students.length } étudiant{ classroom.students.length > 1 ? 's' : '' }</div>
        </div>
    );
}

export default Classroom;
