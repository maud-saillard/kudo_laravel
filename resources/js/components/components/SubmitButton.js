import React, {useEffect, useState} from 'react';

function SubmitButton(props) {
    const [title, setTitle] = useState(props.title ? props.title : 'Sauvegarder');
    const [onLoad, setOnLoad] = useState(false);
    const [disabled, setDisabled] = useState(!!props.disabled);

    useEffect( () => {
        setOnLoad(props.onLoad);
    }, [props.onLoad]);

    useEffect( () => {
        setDisabled(props.disabled);
    }, [props.disabled]);

    function submit(e) {
        e.preventDefault();
        if(!disabled) {
            props.submit(true);
        }
    }

    return (
        <button type="submit" className={`btn-green-full btn-smaller ${(disabled) ? "disabled" : ""}`} onClick={submit}>
            { title }
            {(onLoad) &&
            <div className="spinner-grow spinner-grow-sm text-light" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            }
        </button>
    )
}

export default SubmitButton;
