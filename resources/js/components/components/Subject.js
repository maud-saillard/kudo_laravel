import React from 'react';

function Subject({subject}) {
    return (
        <div className="subject-card">
            <h3>{ subject.name }</h3>
            <div>{ subject.chapters.length } chapitre{ subject.chapters.length > 1 ? 's' : '' }</div>
        </div>
    );
}

export default Subject;
