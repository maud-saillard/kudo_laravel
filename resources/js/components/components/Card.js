import React, {useState, useEffect, useRef} from 'react';
import useForm from "../hooks/useForm";
import SubmitButton from "./SubmitButton";
import Api from "../services/Api";
import {useUserContext} from "../context/UserContext";
import DeleteSubjectModal from "../modals/DeleteSubjectModal";
import Modal from "./Modal";
import useModal from "../hooks/useModal";
import {Editor, EditorState, RichUtils, convertToRaw, convertFromRaw} from 'draft-js';
import 'draft-js/dist/Draft.css';

function Card({classroomIndex = null, card = null, edit = false, editable = false, subjectIndex, chapterIndex, subChapterIndex}) {
    const {user, setUser} = useUserContext();
    const [displayNone, setDisplayNone] = useState(false);
    const [showColors, setShowColors] = useState(false);
    const [iconUpload, setIconUpload] = useState(false);
    const [edited, setEdited] = useState(edit);
    const [onLoad, setOnLoad] = useState(false);
    const [errors, setErrors] = useState([]);
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const [editorState, setEditorState] = React.useState(card ? EditorState.createWithContent(convertFromRaw(JSON.parse(card.content))) : EditorState.createEmpty());
    const fileInput = useRef(null);
    const editor = React.useRef(null);

    const triggerInput = () => { fileInput.current.click() };

    function focusEditor() {
        if(edited) {
            editor.current.focus();
        }
    }

    const initialState = {
        name: card ? card.name : '',
        content: card ? card.content : ''
    };
    const [form, setForm] = useForm(initialState);

    let idSubChapter = null;
    if (classroomIndex !== null) {
        idSubChapter = user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].id;
    } else {
        idSubChapter = user.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].id;
    }

    let cardIndex = null;

    if(card) {
        if (classroomIndex !== null) {
            cardIndex = user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.findIndex(x => x.id === card.id);
        } else {
            cardIndex = user.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.findIndex(x => x.id === card.id);
        }
    }

    function readURL() {
        if (fileInput.current.files && fileInput.current.files[0]) {
            console.log('xdd');
            var reader = new FileReader();

            reader.onload = function(e) {
                setIconUpload(e.target.result);
            };

            reader.readAsDataURL(fileInput.current.files[0]); // convert to base64 string
        }
    }

    function editCard() {
        setEdited(true)
    }

    function submitValue() {
        setOnLoad(true);

        form.content = JSON.stringify(convertToRaw(editorState.getCurrentContent()));

        const data = new FormData();
        data.append('name', form.name);
        data.append('content', form.content);
        if(fileInput.current.files[0]) {
            data.append('file', fileInput.current.files[0]);
        }

        if(card) {
            Api.updateCard(card.id, data).then(response => {
                let newData = {...user};

                if (classroomIndex !== null) {
                    newData.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards[cardIndex] = response.data.data;
                } else {
                    newData.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards[cardIndex] = response.data.data;
                }

                setUser(newData);
                setEdited(false);
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        } else {
            Api.postCard(data, idSubChapter).then(response => {
                let newData = {...user};

                if (classroomIndex !== null) {
                    newData.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.unshift(response.data.data);
                } else {
                    newData.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.unshift(response.data.data);
                }

                setUser(newData);
                setEdited(false);
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function deleteCard() {
        setOnLoad(true);

        if(card) {
            Api.deleteCard(card.id).then(response => {
                let newData = {...user};

                if (classroomIndex !== null) {
                    newData.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.splice(cardIndex, 1);
                } else {
                    newData.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.splice(cardIndex, 1);
                }

                setUser(newData);
                setEdited(false);
                setDisplayNone(true);
                closeDeleteModal();
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        } else {
            setDisplayNone(true);
            setOnLoad(false);
            closeDeleteModal();
        }
    }

    const boldText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'BOLD');
        setEditorState(nextState);
    };

    const italicText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'ITALIC');
        setEditorState(nextState);
    };

    const underlineText = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');
        setEditorState(nextState);
    };

    const greenColor = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'GREEN-COLOR');
        setEditorState(nextState);
    };

    const blueColor = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'BLUE-COLOR');
        setEditorState(nextState);
    };

    const lightBlueColor = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'LIGHT-BLUE-COLOR');
        setEditorState(nextState);
    };

    const greyColor = (e) => {
        e.preventDefault();
        let nextState = RichUtils.toggleInlineStyle(editorState, 'GREY-COLOR');
        setEditorState(nextState);
    };

    const styleMap = {
        'GREEN-COLOR': {
            'color': '#6ad8a3',
        },
        'BLUE-COLOR': {
            'color': '#536df8',
        },
        'LIGHT-BLUE-COLOR': {
            'color': '#adbaff',
        },
        'GREY-COLOR': {
            'color': '#b9cdd5',
        }
    };

    return (
        <div className="kudo-card" style={{ display: displayNone ? 'none': 'block'}}>
            <div className="row">
                <div className="col-6"></div>
                <div className="col-6 pl-0">
                    {!!edited && (
                        <div className="editor-barre">
                            <div>
                                <i className="fas fa-palette pl-1" onMouseDown = {() => setShowColors(!showColors)}></i>
                                <div className={'popover-colors ' + (showColors ? 'show' : '')}>
                                    <span  className={'color green'} onMouseDown = {(e) => greenColor(e)}></span>
                                    <span  className={'color blue'} onMouseDown = {(e) => blueColor(e)}></span>
                                    <span  className={'color light-blue'} onMouseDown = {(e) => lightBlueColor(e)}></span>
                                    <span  className={'color grey'} onMouseDown = {(e) => greyColor(e)}></span>
                                </div>
                                <i className="fas fa-bold" onMouseDown = {(e) => boldText(e)}></i>
                                <i className="fas fa-italic" onMouseDown = {(e) => italicText(e)}></i>
                                <i className="fas fa-underline" onMouseDown = {(e) => underlineText(e)}></i>
                            </div>
                            <div>
                                <i className="fas fa-trash p-0" onClick={openDeleteModal}></i>
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <div className="kudo-card-content">
                <div className="row">
                    <div className="col-6">
                        <div className="title-card">Terme</div>
                        {edited && (
                            <div className="form-group input-no-border">
                                <input type="text" name="name" onChange={setForm} value={form.name}
                                       className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                                       placeholder="Ajouter un terme"/>
                                {(errors && errors.name) &&
                                <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}
                            </div>
                        ) || (
                            <div className="not-editable">{ form.name ? form.name : 'Ajouter un terme' }</div>
                        )}
                    </div>
                    <div className="col-6 pl-0">
                        {!!editable && (
                            !edited && (
                            <span className={'edit-card-button'} onClick={editCard}><i className="fas fa-pen"></i></span>
                            )
                        )}
                        <div className={'editor ' + (card && card.content ? 'content' : '') + (edited ? 'no-content' : '')}>
                            <div className="title-card">Définition</div>
                                <div className={'block-editor'}>
                                    {!!edited && (
                                        <div onClick={triggerInput} className={'card-file'}>
                                            <input type="file" name="file" className={'hide-input' + ((errors && errors.file) ? ' is-invalid' : '')} ref={fileInput} onChange={() => readURL()}/>
                                            {(errors && errors.file) && <span className="invalid-feedback" role="alert"> {errors.file[0]} </span>}
                                            {iconUpload && (
                                                <i className="fas fa-check"></i>
                                            ) || (
                                                <i className="far fa-image"></i>
                                            )}
                                        </div>
                                    )}

                                    <Editor
                                        placeholder={'Ajouter une définition'}
                                        ref={editor}
                                        editorState={editorState}
                                        onChange={editorState => setEditorState(editorState)}
                                        customStyleMap={styleMap}
                                        readOnly={edited ? false : true}
                                    />
                                </div>

                            {!!iconUpload && (
                                <div className={'mt-2'}>
                                    <a target="_blank" className={'d-block w-100 h-100'}>
                                        <img src={iconUpload} title={'File card '} className="w-100"/>
                                    </a>
                                </div>
                            ) || (
                                !!(card && card.file) && (
                                    <div className={'mt-2'}>
                                        <a href={card.file} target="_blank" className={'d-block w-100 h-100'}>
                                            <img src={card.file} title={'File card ' + card.name} className="w-100"/>
                                        </a>
                                    </div>
                                ) || (
                                    <div></div>
                                )
                            )}
                        </div>
                    </div>
                </div>
            </div>

            {!!edited && (
                <div className={'row'}>
                    <div className="col-12 d-flex justify-content-end mt-2">
                        <SubmitButton onLoad={onLoad} submit={() => submitValue()} disabled={!(!!form.name)}/>
                    </div>
                </div>
            )}

            <Modal open={openDelete} close={closeDeleteModal}>
                <div className="grey text-center mb-4">Etes vous sur de supprimer cette carte ?</div>

                <div className="d-flex justify-content-center">
                    <SubmitButton onLoad={onLoad} submit={() => deleteCard()} disabled={false} title={'Supprimer'}/>
                </div>
            </Modal>
        </div>
    );
}

export default Card;
