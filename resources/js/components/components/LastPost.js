import React from 'react';
import {NavLink} from "react-router-dom";

function LastPost({data}) {

    if(data.type === 'subject') {
        return (
            <NavLink to={'/dashboard/classes/' + data.classroom_id + '/sujet/' + data.subject_id} className={'last-posts'}>
                <div className="chapter-card">
                    <div className="vignette"></div>
                    <div className="w-100 d-flex justify-content-between align-items-center py-3 pr-3">
                        <div className="title-chapter">{data.name}</div>
                        <div className="nb-chapters" style={{minWidth: '70px'}}>{data.classroom}</div>
                    </div>
                </div>
            </NavLink>
        );
    }

    if(data.type === 'chapter') {
        return (
            <NavLink to={'/dashboard/classes/' + data.classroom_id + '/sujet/' + data.subject_id + '/chapitre/' + data.id} className={'last-posts'}>
                <div className="chapter-card">
                    <div className="vignette"></div>
                    <div className="w-100 d-flex justify-content-between align-items-center py-3 pr-3">
                        <div className="title-chapter">{data.name}</div>
                        <div className="nb-chapters" style={{minWidth: '70px'}}>{data.classroom}<br/>{data.subject}</div>
                    </div>
                </div>
            </NavLink>
        );
    }

    if(data.type === 'sub-chapter') {
        return (
            <NavLink to={'/dashboard/classes/' + data.classroom_id + '/sujet/' + data.subject_id + '/chapitre/' + data.chapter_id + '/sous-chapitre/' + data.id} className={'last-posts'}>
                <div className="chapter-card">
                    <div className="vignette"></div>
                    <div className="w-100 d-flex justify-content-between align-items-center py-3 pr-3">
                        <div className="title-chapter">{data.name}</div>
                        <div className="nb-chapters" style={{minWidth: '70px'}}>{data.classroom}<br/>{data.subject}</div>
                    </div>
                </div>
            </NavLink>
        );
    }

    return (
        <div></div>
    )
}

export default LastPost;
