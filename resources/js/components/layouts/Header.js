import React, {useState} from 'react';
import {Link, useLocation} from "react-router-dom";
import Animate from "../components/Animate";

function Header() {
    const [showDropdown, setShowDropdown] = useState(false);
    const [showDropdownMenu, setShowDropdownMenu] = useState(false);
    const toggleDropdown = () => { setShowDropdown(!showDropdown); };
    const toggleDropdownMenu = () => { setShowDropdownMenu(!showDropdownMenu); };

    return (
        <header>
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light">

                    <Link className="navbar-brand logo" to={'/'}>
                        <img src={'/images/logo.svg'} alt={'Kudo'}/>
                    </Link>

                    <button className={'navbar-toggler'} type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                            aria-label="Toggle navigation" onClick={toggleDropdownMenu}>
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    {(useLocation().pathname === '/') &&
                    <div className={'collapse navbar-collapse ' + (showDropdownMenu ? 'show' : '')} id="navbarTogglerDemo02">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item">
                                <a className="nav-link dropdown-toggle" type="button"
                                   id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false" onClick={toggleDropdown}>
                                    Découvrir
                                </a>

                                <Animate className={"dropdown-menu"} show={showDropdown}>
                                    <a className="dropdown-item black font-weight-medium"
                                       href="#homepage-presentation-student">Vous êtes un étudiant</a>
                                    <a className="dropdown-item black font-weight-medium"
                                       href="#homepage-presentation-tutorat">Vous êtes une université</a>
                                </Animate>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#contact">Contact</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Téléchargez l’application</a>
                            </li>
                        </ul>

                        {(localStorage.getItem('token')) &&
                        <div className="connect-btn-navbar">
                            <a className="blue-btn" href="/dashboard">
                                Dashboard
                            </a>
                        </div>
                        ||
                        <div className="connect-btn-navbar">
                            <Link className="blue-btn" to={'/login'}>
                                Connexion
                            </Link>
                        </div>
                        }

                    </div>
                    ||
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item">
                                <a className="nav-link active" href="#">Étudiant</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Association ou tutorat</a>
                            </li>
                        </ul>

                        <div className="connect-btn-navbar">
                            <a href="#contact" className="blue-btn">Nous contacter</a>
                        </div>
                    </div>
                    }

                </nav>
            </div>
        </header>
    );
}

export default Header;
