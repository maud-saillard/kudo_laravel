import React from 'react';
import {NavLink} from "react-router-dom";
import useModal from "../../hooks/useModal";
import Modal from "../../components/Modal";
import LogoutModal from "../../modals/LogoutModal";
import { useUserContext } from "../../context/UserContext";

function Header(props) {
    const {user} = useUserContext();
    const { open, openModal, closeModal } = useModal();

    return (
        <menu id="menu-dashboard">
            <div className="h-100">
                <nav>
                    <ul>
                        <div className="logo">
                            <a href="/">
                                <img src={'/images/logo.svg'} alt={'Kudo'}/>
                            </a>
                        </div>
                        <li>
                            <NavLink className="logo" exact={true} to={'/dashboard'} activeClassName="active">
                                <span>Accueil</span> <i className="fas fa-home" title="Accueil"></i>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink className="logo" to={'/dashboard/comment-ca-marche'} activeClassName="active">
                                <span>Comment ça marche ?</span> <i className="fas fa-question-circle"></i>
                            </NavLink>
                        </li>

                        {!!user.tutor &&
                            <li>
                                <NavLink className="logo" to={'/dashboard/classes'} activeClassName="active">
                                    <span>Classes</span> <i className="fas fa-file-alt"></i>
                                </NavLink>
                            </li>
                        }

                        {!!user.student &&
                            <li>
                                <NavLink className="logo" to={'/dashboard/mes-cours'} activeClassName="active">
                                    <span>Mes cours</span> <i className="fas fa-file-alt"></i>
                                </NavLink>
                            </li>
                        }

                        {!!user.student &&
                            <li>
                                <NavLink className="logo" to={'/dashboard/classes'} activeClassName="active">
                                    <span>Mes classes</span> <i className="fas fa-file"></i>
                                </NavLink>
                            </li>
                        }
                    </ul>
                    <ul>
                        <li>
                            <NavLink className="logo" to={'/dashboard/parametres'} activeClassName="active">
                                <span>Paramètres</span> <i className="fas fa-cog"></i>
                            </NavLink>
                        </li>

                        <li>
                            <NavLink className="logo" to={'/dashboard/contact'} activeClassName="active">
                                <span>Contactez-nous</span> <i className="fas fa-envelope"></i>
                            </NavLink>
                        </li>

                        <li onClick={openModal} className="d-inline-block">
                            <div className="d-flex align-items-center justify-content-end" >
                                <span>Déconnexion</span>
                                {!!user.student && (
                                    <div className={'avatar'}>
                                        <img src={user.avatar} title={'Avatar ' + user.name}/>
                                    </div>
                                )}
                            </div>
                        </li>

                        <Modal open={open} close={closeModal}>
                            <LogoutModal/>
                        </Modal>
                    </ul>
                </nav>
            </div>
        </menu>
    );
}

export default Header;
