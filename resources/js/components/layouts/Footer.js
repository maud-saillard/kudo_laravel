import React from 'react';

function Footer() {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-12 mb-4">
                        <a href="/">
                            <img src={'/images/logo-white.svg'} alt={'Kudo'} className="logo"/>
                        </a>
                    </div>
                    <div className="col-lg-5 col-md-5 col-12">
                        <div className="d-flex mb-4 wrap-mobile">
                            <a href="" className="social-network mr-3"><img src={'/images/facebook.svg'} alt={'Faceook'}/></a>
                            <a href="" className="social-network mr-3"><img src={'/images/instagram.svg'} alt={'Instagram'}/></a>
                            <a href="#" className="btn-white">Téléchargez l’application</a>
                        </div>
                        <p>© Tous droits réservés - KUDO - 2020</p>
                    </div>
                    <div className="col-lg-5 col-md-5 col-12 d-flex align-items-end">
                        <p className="text-white">
                            <span className="text-uppercase">Politique de confidentialité</span><br/>
                            <span className="text-uppercase">Mentions légales</span><br/>
                            <span className="">contact@kudo.education</span><br/>
                            <span className="">+33 (0)6 00 00 00 00</span><br/>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
