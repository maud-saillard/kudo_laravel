import React from 'react';
import ReactDOM from 'react-dom';
import { useState, useEffect } from 'react';
import ScrollToTop from "./components/ScrollToTop";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Loader from "./components/Loader";
import Header from "./layouts/dashboard/Header";
import Home from "./pages/dashboard/Home";
import HowItWorks from "./pages/dashboard/HowItWorks";
import Contact from "./pages/dashboard/Contact";
import Settings from "./pages/dashboard/Settings";
import Courses from "./pages/dashboard/Courses";
import Api from "./services/Api";
import { UserProvider } from "./context/UserContext";
import Classrooms from "./pages/dashboard/Classrooms";
import DeleteAccountModal from "./modals/DeleteAccountModal";
import Modal from "./components/Modal";
import useModal from "./hooks/useModal";
import SendVerificationEmailModal from "./modals/SendVerificationEmailModal";

function Dashboard() {
    const [authenticated, setAuthenticated] = useState(false);
    const [user, setUser] = useState([]);
    const [verificationEmail, setVerificationEmail] = useState(false);
    const { open, openModal } = useModal();

    useEffect( () => {
        if(localStorage.getItem('token')) {
            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
            Api.getUser().then(response => {
                setUser(response.data.data);
                setAuthenticated(true);
                console.log('Authenticated');
            }).catch(error => {
                console.log(error);
                if (error.response.status === 422) {
                    console.log(error.response.data.errors || {});
                }

                if (error.response.status === 401) {
                    localStorage.removeItem('token');
                    window.location.replace('/');
                }

                if (error.response.status === 403) {
                    setVerificationEmail(true);
                    openModal(true);
                }
            });
        } else {
            window.location.replace('/');
        }
    }, []);

    function closeModal() {
        localStorage.removeItem('token');
        window.location.replace('/');
    }

    if(verificationEmail) {
        return (
            <Modal open={open} close={closeModal}>
                <SendVerificationEmailModal user={user}/>
            </Modal>
        )
    }

    return (
        (authenticated &&
            <UserProvider user={user}>
                <Router>
                    <ScrollToTop/>
                    <Header/>
                    <main>
                        <Switch>
                            <Route path="/dashboard/classes" component={Classrooms}/>
                            <Route path="/dashboard/mes-cours" component={Courses}/>
                            <Route path="/dashboard/contact" component={Contact}/>
                            <Route path="/dashboard/comment-ca-marche" component={HowItWorks}/>
                            <Route path="/dashboard/parametres" component={Settings}/>
                            <Route path="/dashboard" component={Home}/>
                        </Switch>
                    </main>
                </Router>
            </UserProvider>
            ||
            <Loader/>
        )
    );
}

export default Dashboard;

if (document.getElementById('dashboard')) {
    ReactDOM.render(<Dashboard />, document.getElementById('dashboard'));
}
