import React from 'react';
import { useState } from 'react';
import {Link} from "react-router-dom";
import Api from "../services/Api";
import useForm from "../hooks/useForm";

function RegisterForm() {
    const initialState = {
        name: '',
        email: '',
        password: ''
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess('');
        setOnLoad(false);
        setErrors([]);
    };

    const submitValue = (e) => {
        e.preventDefault();

        setOnLoad(true);

        Api.register(form).then(response => {
            clearForm();
            setSuccess('Inscription réussie ! Veuillez valider votre email pour continuer');
            if(response.data) {
                localStorage.setItem('token', response.data);
                window.location.replace("/dashboard");
            }
        }).catch(error => {
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <form className={'homepage-form grey-form'}>
            <div className="form-group">
                <input type="text" name="name" onChange={setForm} value={form.name} className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')} placeholder="Nom Prénom"/>
                { (errors && errors.name) && <span className="invalid-feedback" role="alert"> {errors.name[0] } </span> }
            </div>
            <div className="form-group">
                <input type="email" name="email" onChange={setForm} value={form.email} className={'form-control' + ((errors && errors.email) ? ' is-invalid' : '')} placeholder="Adresse mail"/>
                { (errors && errors.email) && <span className="invalid-feedback" role="alert"> {errors.email[0] } </span> }
            </div>
            <div className="form-group">
                <input type="password" name="password" onChange={setForm} value={form.password} className={'form-control' + ((errors && errors.password) ? ' is-invalid' : '')} placeholder="Mot de passe"/>
                { (errors && errors.password) && <span className="invalid-feedback" role="alert"> {errors.password[0] } </span> }
            </div>
            <div className="row">
                <div className="col-6 d-flex justify-content-center align-items-center">
                    <Link className="grey" to={'/reset-password'}>Mot de passe oublié ?</Link>
                </div>
                <div className="col-6">
                    <button type="submit" className="btn-green-full" onClick={submitValue}>
                        Inscription
                        { (onLoad) &&
                         <div className="spinner-grow spinner-grow-sm text-light" role="status">
                            <span className="sr-only">Loading...</span>
                         </div>
                        }
                    </button>
                </div>

                <div className="col-12 mt-4">
                    <span className="green font-weight-bold">
                        {success}
                    </span>
                </div>
            </div>
        </form>
    );
}

export default RegisterForm;
