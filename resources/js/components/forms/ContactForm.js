import React from 'react';
import { useState } from 'react';
import Api from "../services/Api";
import useForm from "../hooks/useForm";

function ContactForm() {
    const initialState = {
        name: '',
        email: '',
        university: '',
        message: ''
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess('');
        setErrors([]);
    };

    const submitValue = (e) => {
        e.preventDefault();

        setOnLoad(true);

        Api.postContact(form).then(response => {
            clearForm();
            setSuccess('Message envoyé !');
        }).catch(error => {
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <form className={'homepage-form'}>
            <div className="form-group">
                <input type="text" name="name" onChange={setForm} value={form.name} className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')} placeholder="Nom Prénom"/>
                { (errors && errors.name) && <span className="invalid-feedback" role="alert"> {errors.name[0] } </span> }
            </div>
            <div className="form-group">
                <input type="email" name="email" onChange={setForm} value={form.email} className={'form-control' + ((errors && errors.email) ? ' is-invalid' : '')} placeholder="Adresse mail"/>
                { (errors && errors.email) && <span className="invalid-feedback" role="alert"> {errors.email[0] } </span> }
            </div>
            <div className="form-group">
                <input type="text" name="university" onChange={setForm} value={form.university} className={'form-control' + ((errors && errors.university) ? ' is-invalid' : '')} placeholder="Université"/>
                { (errors && errors.university) && <span className="invalid-feedback" role="alert"> {errors.university[0] } </span> }
            </div>
            <div className="form-group">
                <textarea name="message" onChange={setForm} value={form.message} className={'form-control' + ((errors && errors.message) ? ' is-invalid' : '')} placeholder="Message" rows="5">{form.message}</textarea>
                { (errors && errors.message) && <span className="invalid-feedback" role="alert"> {errors.message[0] } </span> }
            </div>
            <div className="row">
                <div className="col-6 d-flex justify-content-center align-items-center">
                    <span className="green font-weight-bold">
                        {success}
                    </span>
                </div>
                <div className="col-6 d-flex justify-content-end">
                    <button type="submit" className="btn-green-full" onClick={submitValue}>
                        Envoyer
                        { (onLoad) &&
                         <div className="spinner-grow spinner-grow-sm text-light" role="status">
                            <span className="sr-only">Loading...</span>
                         </div>
                        }
                    </button>
                </div>
            </div>
        </form>
    );
}

export default ContactForm;
