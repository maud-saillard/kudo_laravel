import React from 'react';
import { useState } from 'react';
import {Link} from "react-router-dom";
import Api from "../services/Api";
import useForm from "../hooks/useForm";
import SubmitButton from "../components/SubmitButton";

function NewPasswordForm({token, email}) {
    const initialState = {
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess('');
        setOnLoad(false);
        setErrors([]);
    };

    const submitValue = () => {
        setOnLoad(true);

        localStorage.removeItem('token');

        Api.resetPassword(form).then(response => {
            clearForm();
            setSuccess(response.data.message);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <form className={'homepage-form grey-form'}>
            <div className="form-group">
                <span className={'' + ((errors && errors.email) ? ' is-invalid' : '')}></span>
                { (errors && errors.email) && <span className="invalid-feedback" role="alert"> {errors.email[0] } </span> }
            </div>
            <div className="form-group">
                <input type="password" name="password" onChange={setForm} value={form.password} className={'form-control' + ((errors && errors.password) ? ' is-invalid' : '')} placeholder="Nouveau mot de passe"/>
                { (errors && errors.password) && <span className="invalid-feedback" role="alert"> {errors.password[0] } </span> }
            </div>
            <div className="form-group">
                <input type="password" name="password_confirmation" onChange={setForm} value={form.password_confirmation} className={'form-control' + ((errors && errors.password_confirmation) ? ' is-invalid' : '')} placeholder="Confirmer le mot de passe"/>
                { (errors && errors.password_confirmation) && <span className="invalid-feedback" role="alert"> {errors.password_confirmation[0] } </span> }
            </div>
            <div className="row">
                <div className="col-6 d-flex justify-content-center align-items-center">
                    <Link className="grey" to={'/login'}>Se connecter</Link>
                </div>
                <div className="col-6">
                    <SubmitButton title={'Sauvegarder'} onLoad={onLoad} submit={() => submitValue()} className="mx-auto"/>
                </div>

                <div className="col-12 mt-3">
                    <span className="green font-weight-bold mt-3">
                        {success}
                    </span>
                </div>
            </div>
        </form>
    );
}

export default NewPasswordForm;
