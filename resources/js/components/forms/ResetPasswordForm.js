import React from 'react';
import { useState } from 'react';
import {Link} from "react-router-dom";
import Api from "../services/Api";
import useForm from "../hooks/useForm";
import SubmitButton from "../components/SubmitButton";

function ResetPasswordForm() {
    const initialState = {
        email: '',
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess('');
        setOnLoad(false);
        setErrors([]);
    };

    const submitValue = () => {
        setOnLoad(true);

        localStorage.removeItem('token');

        Api.AskResetPassword(form).then(response => {
            clearForm();
            setSuccess(response.data.message);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <form className={'homepage-form grey-form'}>
            <div className="grey mb-4 mt-2 font-size-12">Nous vous enverrons un lien de réinitialisation sur votre adresse mail : </div>
            <div className="form-group">
                <input type="email" name="email" onChange={setForm} value={form.email} className={'form-control' + ((errors && errors.email) ? ' is-invalid' : '')} placeholder="Adresse mail"/>
                { (errors && errors.email) && <span className="invalid-feedback" role="alert"> {errors.email[0] } </span> }
            </div>
            <div className="row">
                <div className="col-6 d-flex justify-content-center align-items-center">
                    <Link className="grey" to={'/login'}>Se connecter</Link>
                </div>
                <div className="col-6">
                    <SubmitButton title={'Envoyer'} onLoad={onLoad} submit={() => submitValue()} className="mx-auto"/>
                </div>

                <div className="col-12 mt-3">
                    <span className="green font-weight-bold mt-3">
                        {success}
                    </span>
                </div>
            </div>
        </form>
    );
}

export default ResetPasswordForm;
