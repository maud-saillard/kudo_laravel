import React from 'react';
import { useState } from 'react';
import {Link} from "react-router-dom";
import Api from "../services/Api";
import useForm from "../hooks/useForm";

function LoginForm() {
    const initialState = {
        email: '',
        password: '',
        device_name: 'website-' + Math.floor(Math.random() * 1000)
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess('');
        setOnLoad(false);
        setErrors([]);
    };

    const submitValue = (e) => {
        e.preventDefault();

        setOnLoad(true);

        localStorage.removeItem('token');

        Api.login(form).then(response => {
            clearForm();
            if(response.data) {
                localStorage.setItem('token', response.data);
                if(localStorage.getItem('token')) {
                    window.location.replace("/dashboard");
                }
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <form className={'homepage-form grey-form'}>
            <div className="form-group">
                <input type="email" name="email" onChange={setForm} value={form.email} className={'form-control' + ((errors && errors.email) ? ' is-invalid' : '')} placeholder="Adresse mail"/>
                { (errors && errors.email) && <span className="invalid-feedback" role="alert"> {errors.email[0] } </span> }
            </div>
            <div className="form-group">
                <input type="password" name="password" onChange={setForm} value={form.password} className={'form-control' + ((errors && errors.password) ? ' is-invalid' : '')} placeholder="Mot de passe"/>
                { (errors && errors.password) && <span className="invalid-feedback" role="alert"> {errors.password[0] } </span> }
            </div>
            <div className="row">
                <div className="col-6 d-flex justify-content-center align-items-center">
                    <Link className="grey" to={'/reset-password'}>Mot de passe oublié ?</Link>
                </div>
                <div className="col-6">
                    <button type="submit" className="btn-green-full" onClick={submitValue}>
                        Connexion
                        { (onLoad) &&
                         <div className="spinner-grow spinner-grow-sm text-light" role="status">
                            <span className="sr-only">Loading...</span>
                         </div>
                        }
                    </button>
                </div>
            </div>
        </form>
    );
}

export default LoginForm;
