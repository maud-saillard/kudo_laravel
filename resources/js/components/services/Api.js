class Api {
    async register(data) {
        return await axios.post('/api/register', data);
    }

    async getUserStats(id = null) {
        if(id) {
            return await axios.get('/api/user/stats/' + id);
        }
        return await axios.get('/api/user/stats');
    }

    async getLastPosts(id = null) {
        if(id) {
            return await axios.get('/api/user/last-posts/' + id);
        }
        return await axios.get('/api/user/last-posts');
    }

    async sendVerificationEmail() {
        return await axios.post('/api/send-verification-email');
    }

    async AskResetPassword(data) {
        return await axios.post('/api/password/email', data);
    }

    async resetPassword(data) {
        return await axios.post('/api/password/reset', data);
    }

    async login(data) {
        return await axios.post('/api/login', data);
    }

    async logout(data) {
        return await axios.post('/api/logout', data);
    }

    async getUser() {
        return await axios.get("/api/user");
    }

    async updateUser(id, data) {
        return await axios.put('/api/users/' + id, data);
    }

    async deleteUser(id) {
        return await axios.delete('/api/users/' + id);
    }

    async updateUserAvatar(id, data) {
        return await axios.post('/api/users/avatar/' + id, data);
    }

    async postContact(data) {
        return await axios.post('/api/contact', data);
    }

    async getClassrooms() {
        return await axios.get('/api/classrooms');
    }

    async postClassroom(data) {
        return await axios.post('/api/classrooms', data);
    }

    async updateClassroom(idClassroom, data) {
        return await axios.put('/api/classrooms/' + idClassroom, data);
    }

    async updateClassroomAdmin(idClassroom, data) {
        return await axios.put('/api/classrooms/admin/' + idClassroom, data);
    }

    async addStudents(idClassroom, data) {
        return await axios.put('/api/classrooms/students/' + idClassroom, data);
    }

    async removeStudent(idClassroom, idStudent) {
        return await axios.delete('/api/classrooms/students/' + idClassroom + '/' + idStudent);
    }

    async deleteClassroom(idClassroom) {
        return await axios.delete('/api/classrooms/' + idClassroom);
    }

    async getSubject(idSubject) {
        return await axios.get('/api/subjects/' + idSubject);
    }

    async updateSubject(idSubject, data) {
        return await axios.put('/api/subjects/' + idSubject, data);
    }

    async deleteSubject(idSubject) {
        return await axios.delete('/api/subjects/' + idSubject);
    }

    async getSubjects() {
        return await axios.get('/api/subjects');
    }

    async postSubject(data) {
        return await axios.post('/api/subjects', data);
    }

    async postChapter(data, id) {
        return await axios.post('/api/chapters/' + id, data);
    }

    async updateChapter(idChapter, data) {
        return await axios.put('/api/chapters/' + idChapter, data);
    }

    async updateChapterOrder(idChapter, data) {
        return await axios.put('/api/chapters/order/' + idChapter, data);
    }

    async deleteChapter(idChapter) {
        return await axios.delete('/api/chapters/' + idChapter);
    }

    async postSubChapter(data, idChapter) {
        return await axios.post('/api/sub-chapters/' + idChapter, data);
    }

    async updateSubChapter(idSubChapter, data) {
        return await axios.put('/api/sub-chapters/' + idSubChapter, data);
    }

    async updateSubChapterOrder(idSubChapter, data) {
        return await axios.put('/api/sub-chapters/order/' + idSubChapter, data);
    }

    async deleteSubChapter(idSubChapter) {
        return await axios.delete('/api/sub-chapters/' + idSubChapter);
    }

    async postCard(data, idSubChapter) {
        return await axios.post('/api/cards/' + idSubChapter, data);
    }

    async updateCard(idCard, data) {
        return await axios.post('/api/cards/update/' + idCard, data);
    }

    async updateCardOrder(idCard, data) {
        return await axios.put('/api/cards/order/' + idCard, data);
    }

    async deleteCard(idCard) {
        return await axios.delete('/api/cards/' + idCard);
    }

    async updateCardFile(idCard, data) {
        return await axios.post('/api/cards/file/' + idCard, data);
    }
}

export default new Api();
