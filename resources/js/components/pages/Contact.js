import React from 'react';
import ContactForm from '../forms/ContactForm';

function Contact() {
    return (
        <section id={'contact'}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-6 col-12 d-flex flex-column justify-content-center mb-4">
                        <h2 className="mb-4">Contactez-nous !</h2>
                        <p className={'grey'}>
                            Découvrez comment booster les révisions de
                            vos étudiants avec Kudo.<br/>
                            <br/>
                            Remplissez le formulaire ci-joint, un de nos
                            conseiller vous contactera très rapidement.
                        </p>
                    </div>
                    <div className="col-lg-5 col-md-6 col-12">
                        <ContactForm/>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Contact;
