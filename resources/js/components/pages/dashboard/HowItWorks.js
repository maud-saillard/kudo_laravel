import React from 'react';
import {useUserContext} from "../../context/UserContext";

function HowItWorks() {
    const {user} = useUserContext();

    if(user.tutor) {
        return (
            <section id="how-it-works">
                <div className="d-flex">
                    <div className="">
                        <img src="/images/11_illu_université.png" className="illustration"/>
                    </div>
                    <div className="ml-5 description">
                        <h3>Créez votre classe pour réviser à plusieurs.</h3>

                        <p>Créez une classe et donnez-en l’accès à votre groupe de travail
                            grâce à un lien sécurisé envoyé par mail. Les étudiants de la classe
                            pourrons réviser vos cartes sur l’application.</p>
                    </div>
                </div>

                <div className="d-flex">
                    <div className="">
                        <img src="/images/12_illu_université.png" className="illustration"/>
                    </div>
                    <div className="ml-5 description">
                        <h3>Permettez à chacun d’ajouter des cours tout en gardant le contrôle.</h3>

                        <p>Donnez des accès à durée limitée aux membres de votre groupe
                            pour ajouter des cours. Votre rôle d’administrateur vous donne
                            la possibilité de garder la main et de gérer les contenus.</p>
                    </div>
                </div>

                <div className="d-flex">
                    <div className="">
                        <img src="/images/13_illu_université.png" className="illustration"/>
                    </div>
                    <div className="ml-5 description">
                        <h3>Motivez-vous en équipe et gagnez du temps</h3>

                        <p>Les classes sont un gain de temps. Co-construisez vos révisions
                            sur le site avec vos camarades et challengez-vous sur les connaissances !</p>
                    </div>
                </div>

            </section>
        );
    }

    return (
        <section id="how-it-works">
            <div className="d-flex">
                <div className="">
                    <img src="/images/08_illu_étudiant.png" className="illustration"/>
                </div>
                <div className="ml-5 description">
                    <h3>Je crée mes fiches de révision sur l’application ou via le site</h3>

                    <p>Je crée mes fiches de révision sur l’application ou encore
                        plus simplement sur le site avec un éditeur de texte complet
                        et la possibilité d’ajouter des images. <b>Puis je révise sur mon application !</b></p>
                </div>
            </div>

            <div className="d-flex">
                <div className="">
                    <img src="/images/09_illu_étudiant.png" className="illustration"/>
                </div>
                <div className="ml-5 description">
                    <h3>J’ajoute des cours pour les autres élèves de ma classe</h3>

                    <p>Si mon université est partenaire je peux ajouter via le site des
                        cours pour les autres étudiants de ma classe grâce au
                        code d’accès fourni par mon université. </p>
                </div>
            </div>

            <div className="d-flex">
                <div className="">
                    <img src="/images/10_illu_étudiant.png" className="illustration"/>
                </div>
                <div className="ml-5 description">
                    <h3>Je peux rejoindre une classe pour réviser les cours de mon université</h3>

                    <p>Mon université a créé une « classe » accessible par
                        un code sécurisé qui me permet d’accéder à l’ensemble
                        des cours co-créés par les étudiants de l’université.
                        Je gagne du temps dans mes révisions !</p>
                </div>
            </div>

        </section>
    );
}

export default HowItWorks;
