import React from 'react';
import { useState, useRef } from 'react';
import {Link} from "react-router-dom";
import useForm from '../../hooks/useForm'
import Api from "../../services/Api";
import Modal from "../../components/Modal";
import DeleteAccountModal from "../../modals/DeleteAccountModal";
import useModal from "../../hooks/useModal";
import LogoutModal from "../../modals/LogoutModal";
import SubmitButton from "../../components/SubmitButton";
import {useUserContext} from "../../context/UserContext";

function Settings(props) {
    const {user, setUser} = useUserContext();
    const { open, openModal, closeModal } = useModal();
    const initialState = {
        name: user.name,
        birthday: user.birthday || '1990-01-01',
        email: user.email,
        phone: user.phone || '0600000000',
        password: '',
    };
    const [form, setForm] = useForm(initialState);
    const fileInput = useRef(null);

    const [edit, setEdit] = useState(false);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);
    const [avatarLoading, setAvatarLoading] = useState(false);

    const triggerInput = () => { fileInput.current.click() };

    function editProfil() {
        if(edit) {
            setForm(initialState)
        }
        setEdit(!edit);
    }

    const clearForm = () => {
        setSuccess('');
        setErrors([]);
    };

    function uploadAvatar(file) {
        const data = new FormData();
        data.append('avatar', file);

        setAvatarLoading(true);

        Api.updateUserAvatar(user.id, data).then(response => {
            setUser({
                ...user,
                avatar: response.data.data
            });
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setAvatarLoading(false);
        });
    }

    function submitValue() {

        if(JSON.stringify(initialState) === JSON.stringify(form)) {
            return null;
        }

        setOnLoad(true);

        clearForm();

        Api.updateUser(user.id, form).then(response => {
            setSuccess('Profil modifié !');
            setEdit(false);
            setUser({...user, ...form});
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <section>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h1 className="mb-3">Paramètres</h1>
                    </div>

                    <div className="col-12">
                        <div className="block-white">
                            <div className="title">Informations personnelles</div>

                            <div>

                                {!!user.student && (
                                    <div className="settings-form-group">
                                        <div className="label-group">Photo</div>
                                        <div className="input-group">

                                            <div className={'avatar'} onClick={triggerInput}>
                                                <img src={user.avatar} title={'Avatar ' + user.name} className={avatarLoading ? 'loading' : ''}/>
                                                {avatarLoading && (
                                                    <div className="loader-avatar">
                                                        <div className="spinner-border green" role="status">
                                                            <span className="sr-only">Loading...</span>
                                                        </div>
                                                    </div>
                                                )}
                                            </div>

                                            <input type="file" onChange={(e) => uploadAvatar(e.target.files[0])} className={'hide-input' + ((errors && errors.avatar) ? ' is-invalid' : '')} ref={fileInput} />
                                            {(errors && errors.avatar) && <span className="invalid-feedback" role="alert"> {errors.avatar[0]} </span>}
                                        </div>
                                        <div className="d-flex">
                                            {edit &&
                                            (
                                                <SubmitButton onLoad={onLoad} submit={() => submitValue()}
                                                              disabled={(JSON.stringify(initialState) === JSON.stringify(form))}/>
                                            )}

                                            <div className="edit-button ml-3" onClick={editProfil}><i
                                                className="fas fa-pen"></i></div>
                                        </div>
                                    </div>
                                )}

                                <div className="settings-form-group">
                                    <div className="label-group">{ user.tutor ? 'Nom du tutorat' : 'Prénom Nom'}</div>
                                    <div className="input-group">
                                        {edit &&
                                        (
                                            <input type="text" name="name" onChange={setForm} value={form.name}
                                               className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                                               placeholder="Nom Prénom"/>
                                        )}
                                            {(errors && errors.name) && <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}

                                        {!edit &&
                                            <div className="before-edit">{form.name}</div>
                                        }
                                    </div>
                                </div>

                                {!!user.student && (
                                    <div className="settings-form-group">
                                        <div className="label-group">Date de naissance</div>
                                        <div className="input-group">
                                            {edit &&
                                            (
                                                <input type="date" name="birthday" onChange={setForm} value={form.birthday}
                                                       className={'form-control' + ((errors && errors.birthday) ? ' is-invalid' : '')}/>
                                            )}
                                            {(errors && errors.birthday) &&
                                            <span className="invalid-feedback" role="alert"> {errors.birthday[0]} </span>}

                                            {!edit &&
                                            <div className="before-edit">{form.birthday}</div>
                                            }
                                        </div>
                                    </div>
                                )}

                                <div className="settings-form-group no-border">
                                    <div className="label-group">Mot de passe</div>
                                    <div className="input-group">
                                        {edit &&
                                        (
                                            <input type="password" name="password" onChange={setForm} value={form.password}
                                                   className={'form-control' + ((errors && errors.password) ? ' is-invalid' : '')}
                                                   placeholder="****"/>
                                        )}
                                        {(errors && errors.password) &&
                                        <span className="invalid-feedback" role="alert"> {errors.password[0]} </span>}

                                        {!edit &&
                                        <div className="before-edit">******</div>
                                        }
                                    </div>
                                </div>

                                <div className="title mt-4">Coordonnées</div>

                                <div className="settings-form-group">
                                    <div className="label-group">Email</div>
                                    <div className="input-group">
                                        {edit &&
                                        (
                                            <input type="email" name="email" onChange={setForm} value={form.email}
                                                   className={'form-control' + ((errors && errors.email) ? ' is-invalid' : '')}/>
                                        )}
                                        {(errors && errors.email) &&
                                        <span className="invalid-feedback" role="alert"> {errors.email[0]} </span>}

                                        {!edit &&
                                        <div className="before-edit">{form.email}</div>
                                        }
                                    </div>
                                </div>

                                <div className="settings-form-group no-border">
                                    <div className="label-group">Téléphone</div>
                                    <div className="input-group">
                                        {edit &&
                                        (
                                            <input type="tel" name="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                                                   onChange={setForm} value={form.phone}
                                                   className={'form-control' + ((errors && errors.phone) ? ' is-invalid' : '')}/>
                                        )}
                                        {(errors && errors.phone) &&
                                        <span className="invalid-feedback" role="alert"> {errors.phone[0]} </span>}

                                        {!edit &&
                                        <div className="before-edit">{form.phone}</div>
                                        }
                                    </div>
                                </div>

                                <div className="d-flex justify-content-between align-items-center mt-3">
                                    <span className="green font-weight-bold">
                                        {success}
                                    </span>
                                </div>

                            </div>
                        </div>
                    </div>

                    {!!user.student && (
                    <div className="col-lg-5 col-12 mt-3">
                        <div className="block-white">
                            <div className="title">Premium</div>

                            <div className="mt-4 d-flex mb-4">
                                <div className="mr-2 pr-2 border-right-separation">
                                    <span className="green font-weight-bold font-size-20">14,99 €</span><br/>
                                    <span className="black text-uppercase font-weight-bold font-size-12">par an</span><br/>
                                </div>

                                <div>
                                    <span className="green font-weight-bold font-size-20">28,99€</span><br/>
                                    <span className="black text-uppercase font-weight-bold font-size-12">pour toute la vie !</span><br/>
                                </div>
                            </div>

                            <div className="premium">
                                <div>
                                    <i className="fas fa-search"></i>
                                    <span>Moteur de recherche</span>
                                </div><

                                div>
                                    <i className="fas fa-download"></i>
                                    <span>Espace de stockage illimité</span>
                                </div>

                                <div>
                                    <i className="fas fa-share-alt"></i>
                                    <span>Partagez vos cours </span>
                                </div>

                                <div>
                                    <i className="fas fa-toggle-on"></i>
                                    <span>Mode hors connexion</span>
                                </div>

                                <div>
                                    <i className="fas fa-font"></i>
                                    <span>Editeur de texte complet</span>
                                </div>

                                <div>
                                    <i className="fas fa-image"></i>
                                    <span>Scanez vos cours</span>
                                </div>

                                <div>
                                    <i className="fas fa-microphone"></i>
                                    <span>Ajouter des audios</span>
                                </div>
                            </div>

                            <div className="col-12">
                                <button type="submit" className="btn-green-full btn-smaller mx-auto p-2">
                                    Abonnez vous sur l'app !
                                </button>
                            </div>
                        </div>
                    </div>
                    )}

                    <div className="col-lg-7 col-12 mt-3">
                        <div className="block-white">
                            <div className="mb-4"><Link to={'cgu'} className="black font-weight-medium">Politique de confidentialité</Link></div>
                            <div className="mb-4"><Link to={'mentions-legales'} className="black font-weight-medium">Mentions légales</Link></div>
                            <div className="mb-4">
                                <span className="black font-weight-medium cursor-pointer text-underline-hover" onClick={openModal}>
                                    Supprimez votre compte définitivement
                                </span>
                            </div>

                            <Modal open={open} close={closeModal}>
                                <DeleteAccountModal user={user}/>
                            </Modal>
                        </div>
                    </div>

                    <div className="col-12 light-grey mt-4">
                        © Tous droits réservés - KUDO - 2020
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Settings;
