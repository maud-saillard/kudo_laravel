import React from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import CreateSubjectModal from "../../../modals/CreateSubjectModal";
import {NavLink} from "react-router-dom";
import Subject from "../../../components/Subject";
import {useUserContext} from "../../../context/UserContext";

function SubjectsList({subjects}) {
    const {user} = useUserContext();
    const { open, openModal, closeModal } = useModal();

    return (
        <section id="courses" className="container">
            <div className="row">
                <div className="col-12">
                    <h1 className="dark mb-4">
                        Complétez vos cours,<br/>
                        et retrouvez-les sur l’application.
                    </h1>
                </div>

                <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div className="new-school-subject" onClick={openModal}>
                        <i className="fas fa-plus"></i>
                    </div>
                </div>

                {!subjects || subjects.length === 0 && (
                    <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                        <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                        <div className="grey">Commencez par créer une matière.</div>
                    </div>
                )}

                {
                    Object.values(subjects).map((subject) => {
                        return (
                            <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12" key={subject.id}>
                                <NavLink to={'/dashboard/mes-cours/' + subject.id} className="text-decoration-none">
                                    <Subject subject={subject}/>
                                </NavLink>
                            </div>
                        )
                    })
                }

                <Modal open={open} close={closeModal}>
                    <CreateSubjectModal close={closeModal}/>
                </Modal>

            </div>
        </section>
    );
}

export default SubjectsList;
