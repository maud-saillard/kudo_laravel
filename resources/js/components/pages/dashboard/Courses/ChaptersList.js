import React, {useEffect, useState} from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import CreateChapterModal from "../../../modals/CreateChapterModal";
import DeleteSubjectModal from "../../../modals/DeleteSubjectModal";
import EditSubjectModal from "../../../modals/EditSubjectModal";
import {NavLink} from "react-router-dom";
import Chapter from "../../../components/Chapter";
import {useUserContext} from "../../../context/UserContext";
import EditButton from "../../../components/EditButton";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import Api from "../../../services/Api";
import { reorder } from "../../../services/Helper";

function ChaptersList({}) {
    const {user, setUser} = useUserContext();
    const [subjectId] = useState(parseInt(useParams().id));
    const [subjectIndex] = useState(user.subjects.findIndex(x => x.id === subjectId));
    const { open, openModal, closeModal } = useModal();
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const { open: openEdit, openModal: openEditModal, closeModal: closeEditModal } = useModal();

    function onDragEnd(result) {
        if (!result.destination) {
            return;
        }

        const items = reorder(
            user.subjects[subjectIndex].chapters,
            result.source.index,
            result.destination.index
        );

        let newUser = {...user};
        newUser.subjects[subjectIndex].chapters = items;
        setUser(newUser);

        Api.updateChapterOrder(result.draggableId, {order: result.destination.index}).then(response => {
            console.log(response);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        (user.subjects[subjectIndex] && (
            <section id="courses" className="container">
                <div className="row">
                    <div className="col-12">
                        <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/mes-cours'}>
                            Tous mes cours
                        </NavLink>
                        <div className="d-flex justify-content-between align-items-center">
                            <h1 className="dark mb-4 mt-3">
                                { user.subjects[subjectIndex].name }
                            </h1>

                            <EditButton edit={openEditModal} destroy={openDeleteModal}/>
                        </div>
                    </div>

                    <div className="col-12 d-flex align-items-center mb-5">
                        <div className="add-button-chapters" onClick={openModal}><i className="fas fa-plus"></i></div>

                        {!user.subjects[subjectIndex].chapters || user.subjects[subjectIndex].chapters.length === 0 && (
                            <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                                <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                                <div className="grey">Commencez par créer un chapitre.</div>
                            </div>
                        )}
                    </div>

                    {user.subjects[subjectIndex].chapters.length !== 0 && (
                        <div className="col-12">
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="droppable">
                                    {(provided, snapshot) => (
                                        <div
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                        >
                                            {Object.entries(user.subjects[subjectIndex].chapters).map(([key, chapter]) => (
                                                <Draggable key={chapter.id} draggableId={chapter.id.toString()} index={parseInt(key)}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <NavLink to={'/dashboard/mes-cours/' + subjectId + '/chapitre/' + chapter.id} key={chapter.id} className="text-decoration-none">
                                                                <Chapter chapter={chapter}/>
                                                            </NavLink>
                                                        </div>
                                                    )}
                                                </Draggable>
                                            ))}
                                            {provided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                            </DragDropContext>
                        </div>
                    )}

                    <Modal open={open} close={closeModal}>
                        <CreateChapterModal subjectIndex={subjectIndex} close={closeModal}/>
                    </Modal>

                    <Modal open={openDelete} close={closeDeleteModal}>
                        <DeleteSubjectModal subjectIndex={subjectIndex}/>
                    </Modal>

                    <Modal open={openEdit} close={closeEditModal}>
                        <EditSubjectModal subjectIndex={subjectIndex} closeModal={closeEditModal}/>
                    </Modal>

                </div>
            </section>
        ) ||
            <Redirect to="/dashboard/mes-cours"/>
        )
    );
}

export default ChaptersList;
