import React, {useState} from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import DeleteChapterModal from "../../../modals/DeleteChapterModal";
import EditChapterModal from "../../../modals/EditChapterModal";
import {NavLink} from "react-router-dom";
import SubChapter from "../../../components/SubChapter";
import {useUserContext} from "../../../context/UserContext";
import EditButton from "../../../components/EditButton";
import CreateSubChapterModal from "../../../modals/CreateSubChapterModal";
import Api from "../../../services/Api";
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import { reorder } from "../../../services/Helper";

function SubChaptersList({}) {
    const {user, setUser} = useUserContext();
    const [subjectId] = useState(parseInt(useParams().idSubject));
    const [chapterId] = useState(parseInt(useParams().idChapter));
    const { open, openModal, closeModal } = useModal();
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const { open: openEdit, openModal: openEditModal, closeModal: closeEditModal } = useModal();
    const [subjectIndex] = useState(user.subjects.findIndex(x => x.id === subjectId));

    if(!user.subjects[subjectIndex]) {
        return <Redirect to="/dashboard/mes-cours"/>
    }

    const [chapterIndex] = useState(user.subjects[subjectIndex].chapters.findIndex(x => x.id === chapterId));

    if(!user.subjects[subjectIndex].chapters[chapterIndex]) {
        return <Redirect to={'/dashboard/mes-cours/' + user.subjects[subjectIndex].id}/>
    }

    function onDragEnd(result) {
        if (!result.destination) {
            return;
        }

        const items = reorder(
            user.subjects[subjectIndex].chapters[chapterIndex].subChapters,
            result.source.index,
            result.destination.index
        );

        let newUser = {...user};
        newUser.subjects[subjectIndex].chapters[chapterIndex].subChapters = items;
        setUser(newUser);

        Api.updateSubChapterOrder(result.draggableId, {order: result.destination.index}).then(response => {
            console.log(response);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        <section id="courses" className="container">
            <div className="row">
                <div className="col-12">
                    <div>
                        <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/mes-cours'}>
                            Tous mes cours
                        </NavLink>
                        <span className="grey text-uppercase font-size-12"> - </span>
                        <NavLink className="grey text-uppercase font-size-12"
                                 to={'/dashboard/mes-cours/' + user.subjects[subjectIndex].id}>
                            {user.subjects[subjectIndex].name}
                        </NavLink>
                    </div>
                    <div className="d-flex justify-content-between align-items-center">
                        <h1 className="dark mb-4 mt-3">
                            {user.subjects[subjectIndex].chapters[chapterIndex].name}
                        </h1>

                        <EditButton edit={openEditModal} destroy={openDeleteModal}/>
                    </div>
                </div>

                <div className="col-12 d-flex align-items-center mb-5">
                    <div className="add-button-chapters" onClick={openModal}><i className="fas fa-plus"></i></div>

                    {user.subjects[subjectIndex].chapters[chapterIndex].subChapters.length === 0 && (
                        <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                            <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                            <div className="grey">Ajouter un sous-chapitre puis vos cartes !</div>
                        </div>
                    )}
                </div>

                {user.subjects[subjectIndex].chapters[chapterIndex].subChapters.length !== 0 && (
                    <div className="col-12">
                        <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div
                                        {...provided.droppableProps}
                                        ref={provided.innerRef}
                                    >
                                        {Object.entries(user.subjects[subjectIndex].chapters[chapterIndex].subChapters).map(([key, subChapter]) => (
                                            <Draggable key={subChapter.id} draggableId={subChapter.id.toString()} index={parseInt(key)}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                    >
                                                        <NavLink to={'/dashboard/mes-cours/' + subjectId + '/chapitre/' + chapterId + '/sous-chapitre/' + subChapter.id} key={subChapter.id}
                                                                 className="text-decoration-none">
                                                            <SubChapter subChapter={subChapter}/>
                                                        </NavLink>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </div>
                )}

                <Modal open={open} close={closeModal}>
                    <CreateSubChapterModal subjectIndex={subjectIndex} chapterIndex={chapterIndex} close={closeModal}/>
                </Modal>

                <Modal open={openDelete} close={closeDeleteModal}>
                    <DeleteChapterModal subjectIndex={subjectIndex} chapterIndex={chapterIndex}/>
                </Modal>

                <Modal open={openEdit} close={closeEditModal}>
                    <EditChapterModal subjectIndex={subjectIndex} chapterIndex={chapterIndex} closeModal={closeEditModal}/>
                </Modal>

            </div>
        </section>
    );
}

export default SubChaptersList;
