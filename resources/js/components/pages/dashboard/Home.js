import React, {useState, useContext, useEffect} from 'react';
import {useUserContext} from "../../context/UserContext";
import {NavLink} from "react-router-dom";
import Api from "../../services/Api";
import LastPost from "../../components/LastPost";

function Home(props) {
    const {user} = useUserContext();
    const [stats, setStats] = useState([]);
    const [lastPosts, setLastPosts] = useState([]);

    useEffect( () => {
        Api.getUserStats().then(response => {
            setStats(response.data.data);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });

        Api.getLastPosts().then(response => {
            setLastPosts(response.data.data);
            console.log(response.data);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });
    },[]);

    if(user.tutor) {
        return (
            <section id="home-dashboard" className="classroom">
                <h1 className="dark mb-4">Hello !</h1>

                <div className="d-flex">
                    <div className="block-white w-100">
                        <div className="title mb-3">Tableau de bord</div>
                        <p className="black font-size-15 mb-3"><b>{ stats.nb_students }</b> étudiant{ stats.nb_students > 1 &&'s' } sui{ stats.nb_students > 1 ? 'vent' : 't' } vos classes</p>
                        <p className="black font-size-15 mb-3"><b>{ stats.nb_classrooms }</b> classe{ stats.nb_classrooms > 1 && 's' } créée{ stats.nb_classrooms > 1 && 's' }</p>
                        <p className="black font-size-15 mb-3"><b>{ stats.nb_subjects }</b> cours créés</p>
                        <p className="black font-size-15 mb-3"><b>{ stats.nb_admins }</b> étudiant{ stats.nb_admins > 1 && 's' } administrateur{ stats.nb_admins > 1 && 's' }</p>
                    </div>
                </div>

                <div className="w-100 mt-5">
                    <p className="grey font-size-12">Derniers ajouts</p>
                    {Object.entries(lastPosts).map(([key, lastPost]) => {
                    return (
                        <LastPost data={lastPost} key={key}/>
                        )
                    })}
                </div>
            </section>
        );
    } else {
        return (
            <section id="home-dashboard">
                <h1 className="dark mb-4">Hello <span className="font-weight-semi-bold">{user.name}</span> !</h1>

                <div className="d-flex">
                    <div className="mr-4 mt-1 grey"><i className="fas fa-arrow-left"></i></div>
                    <div>
                        <h3 className="grey">Bienvenue sur votre espace personnel.</h3>

                        <p className="grey">
                            Vous n’avez pas encore de contenu, créez votre premier cours
                            ou découvrez comment tout fonctionne !
                        </p>
                        <div>
                            <a href="#" className="btn-green-full m-0">Téléchargez l’application</a>
                        </div>
                    </div>
                </div>

                <div>
                    <img src={'/images/kudo_accueil.png'} alt={'Kudo Accueil'} className={'w-100'} style={{maxWidth: '550px'}}/>
                </div>
            </section>
        );
    }
}

export default Home;
