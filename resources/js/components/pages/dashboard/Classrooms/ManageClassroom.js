import React, {useState, useEffect} from 'react';
import useModal from "../../../hooks/useModal";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import {useUserContext} from "../../../context/UserContext";
import {NavLink} from "react-router-dom";
import EditButton from "../../../components/EditButton";
import useForm from "../../../hooks/useForm";
import SubmitButton from "../../../components/SubmitButton";
import Modal from "../../../components/Modal";
import EditClassroomModal from "../../../modals/EditClassroomModal";
import DeleteClassroomModal from "../../../modals/DeleteClassroomModal";
import Api from "../../../services/Api";
import LastPost from "../../../components/LastPost";

function ManageClassroom({}) {
    const {user, setUser} = useUserContext();
    const [classroomId] = useState(parseInt(useParams().id));
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomId));
    const [stats, setStats] = useState([]);
    const [lastPosts, setLastPosts] = useState([]);
    const { open, openModal, closeModal } = useModal();
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const { open: openEdit, openModal: openEditModal, closeModal: closeEditModal } = useModal();

    useEffect( () => {
        Api.getUserStats(classroomId).then(response => {
            setStats(response.data.data);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });

        Api.getLastPosts(classroomId).then(response => {
            setLastPosts(response.data.data);
            console.log(response.data);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });
    },[]);

    var dateTomorrow = new Date();
    dateTomorrow.setDate(dateTomorrow.getDate() + 1);

    const initialState = {
        admin_email: '',
        date_start: new Date().toISOString().substr(0,10),
        date_end: dateTomorrow.toISOString().substr(0,10)
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [error, setError] = useState(null);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setForm(initialState);
        setSuccess(false);
        setErrors([]);
        setError(null);
    };

    const submit = (e) => {
        setOnLoad(true);

        Api.updateClassroomAdmin(classroomId, form).then(response => {
            setForm(initialState);
            let newUser = {...user};
            newUser.classrooms[classroomIndex] = response.data.data;
            setUser(newUser);
            clearForm();
            closeModal();
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
                setError(error.response.data.message || null);
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        (user.classrooms[classroomIndex] && (
                <section id="classroom" className="container">
                    <div className="row">
                        <div className="col-12">
                            <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes'}>
                                Toutes les classes
                            </NavLink>
                            <div className="d-flex justify-content-between align-items-center">
                                <h1 className="dark mb-4 mt-3">
                                    { user.classrooms[classroomIndex].name }
                                </h1>

                                <EditButton edit={openEditModal} destroy={openDeleteModal}/>
                            </div>
                        </div>

                        <div className="col-12">
                            <div className="row">
                                <div className="col-6">
                                    <div className="block-white mb-3">
                                        <div className="title">Tableau de bord</div>
                                        <p className="grey font-size-15 mb-3">
                                            <b>{ stats.nb_subjects } </b>matière{ stats.nb_subjects > 1 && 's' } -
                                            <b> { stats.nb_chapters } </b>chapitre{ stats.nb_chapters > 1 && 's' } -
                                            <b> { stats.nb_sub_chapters } </b>sous-chapitre{ stats.nb_sub_chapters > 1 && 's' }
                                        </p>

                                        <div className="w-100 mt-3 last-posts-with-border">
                                            <p className="grey font-size-12">Derniers ajouts</p>
                                            {Object.entries(lastPosts).map(([key, lastPost]) => {
                                                return (
                                                    <LastPost data={lastPost} key={key}/>
                                                )
                                            })}
                                        </div>

                                        <div className="w-100 text-center">
                                            <NavLink className="btn-green-full btn-smaller mx-auto p-2" to={'/dashboard/classes/'+ classroomId}>
                                                Voir toutes les matières
                                            </NavLink>
                                        </div>
                                    </div>
                                    <div className="block-white">
                                        <div className="title mb-3">Liste des étudiants</div>

                                        <p className="grey font-size-15 mb-3"><b>{ user.classrooms[classroomIndex].students.length }</b> étudiants inscrits</p>

                                        <NavLink className="btn-green-full btn-smaller mx-auto p-2" to={'/dashboard/classes/manage/'+ classroomId +'/students'}>
                                            Gérer la liste des étudiants
                                        </NavLink>
                                    </div>
                                </div>

                                <div className="col-6">
                                    <div className="block-white">
                                        <div>
                                            <div className="title mb-3">étudiant administrateur</div>

                                            {user.classrooms[classroomIndex].admin_id && (
                                                <div className="">
                                                    <div className="black">
                                                        <b>{user.classrooms[classroomIndex].admin_name}</b><br/>
                                                        {user.classrooms[classroomIndex].admin_email}
                                                    </div>
                                                    <div className="grey font-size-12">
                                                        du {user.classrooms[classroomIndex].date_start} au {user.classrooms[classroomIndex].date_end}
                                                    </div>
                                                </div>
                                            )}
                                        </div>

                                        <hr className="mt-5 mb-5"/>

                                        <div>
                                            <div className="title">Désigner un nouvel étudiant administrateur</div>

                                            <p className={'grey font-size-10 mt-3'}>
                                                Donnez l'accès à un un étudiant pour qu'il
                                                puisse ajouter du contenu via le site web.
                                            </p>

                                            <form>
                                                <div className="settings-form-group no-border">
                                                    <input type="email" name="admin_email" onChange={setForm} value={form.admin_email} className={'form-control' + ((errors && errors.admin_email) ? ' is-invalid' : '')} placeholder="Adresse mail"/>
                                                    { (errors && errors.admin_email) && <span className="invalid-feedback" role="alert"> {errors.admin_email[0] } </span> }
                                                </div>

                                                <div className="settings-form-group no-border">
                                                    <div className={'row'}>
                                                        <div className={'col-lg-6 col-md-12 col-sm-12 col-12'}>
                                                            <div className="label-small">Date de début</div>
                                                            <div className="input-group">
                                                                <input type="date" name="date_start" onChange={setForm} value={form.date_start}
                                                                       className={'form-control' + ((errors && errors.date_start) ? ' is-invalid' : '')}/>

                                                                {(errors && errors.date_start) &&
                                                                <span className="invalid-feedback" role="alert"> {errors.date_start[0]} </span>}
                                                            </div>
                                                        </div>
                                                        <div className={'col-lg-6 col-md-12 col-sm-12 col-12'}>
                                                            <div className="label-small">Date de fin</div>
                                                            <div className="input-group">
                                                                <input type="date" name="date_end" onChange={setForm} value={form.date_end}
                                                                       className={'form-control' + ((errors && errors.date_end) ? ' is-invalid' : '')}/>

                                                                {(errors && errors.date_end) &&
                                                                <span className="invalid-feedback" role="alert"> {errors.date_end[0]} </span>}
                                                            </div>
                                                        </div>

                                                        {error && (
                                                            <div className={'col-12 mt-2 text-center error'}>
                                                                { error }
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>

                                                <div className="mt-3">
                                                    <SubmitButton submit={() => submit()} onLoad={onLoad} disabled={!(!!form.admin_email)} title={'Ajouter l\'étudiant'}/>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <Modal open={openDelete} close={closeDeleteModal}>
                            <DeleteClassroomModal classroomIndex={classroomIndex}/>
                        </Modal>

                        <Modal open={openEdit} close={closeEditModal}>
                            <EditClassroomModal classroomIndex={classroomIndex} closeModal={closeEditModal}/>
                        </Modal>
                    </div>
                </section>
            ) ||
            <Redirect to="/dashboard/classes"/>
        )
    );
}

export default ManageClassroom;
