import React, {useState} from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import CreateSubjectModal from "../../../modals/CreateSubjectModal";
import {NavLink} from "react-router-dom";
import Subject from "../../../components/Subject";
import {useUserContext} from "../../../context/UserContext";
import CreateClassroomModal from "../../../modals/CreateClassroomModal";
import Classroom from "../../../components/Classroom";

function ClassroomsList({classrooms}) {
    const {user} = useUserContext();
    const { open, openModal, closeModal } = useModal();

    return (
        <section id="classroom" className="container">
            <div className="row">
                <div className="col-12">
                    <h1 className="dark mb-4">
                        Classes
                    </h1>
                </div>

                {!!user.tutor && (
                    <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <div className="new-school-subject" onClick={openModal}>
                            <i className="fas fa-plus"></i>
                        </div>
                    </div>
                )}

                {!classrooms || classrooms.length === 0 && (
                    <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                        {!!user.tutor && (
                            <div>
                                <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                                <div className="grey">Commencer par créer une classe</div>
                            </div>
                        ) || (
                            <div className="grey">
                                <div>
                                    Vous ne faites partie d’aucune classe.<br/>
                                    Votre université n’utilise pas encore Kudo, vous pensez qu’elle serait intéressée ?
                                </div>
                                <div className={'mt-3'}>
                                    <NavLink to={'/dashboard/contact'} className="btn-green-full m-0">Contactez-nous !</NavLink>
                                </div>

                                <div>
                                    <img src={'/images/kudo_mesclasses.png'} alt={'Kudo Mes classes'} className={'w-100'} style={{maxWidth: '550px'}}/>
                                </div>
                            </div>
                        )}
                    </div>
                ) || (
                    Object.values(classrooms).map((classroom) => {
                        return (
                            <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12" key={classroom.id}>
                                {(user.id === classroom.user_id) && (
                                    <NavLink to={'/dashboard/classes/manage/' + classroom.id} className="text-decoration-none">
                                        <Classroom classroom={classroom}/>
                                    </NavLink>
                                ) || (
                                    <NavLink to={'/dashboard/classes/'+ classroom.id} className="text-decoration-none">
                                        <Classroom classroom={classroom}/>
                                    </NavLink>
                                )}
                            </div>
                        )
                    })
                )}

                <Modal open={open} close={closeModal}>
                    <CreateClassroomModal close={closeModal}/>
                </Modal>

            </div>
        </section>
    );
}

export default ClassroomsList;
