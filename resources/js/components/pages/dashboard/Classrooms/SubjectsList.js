import React, {useState} from 'react';
import Modal from "../../../components/Modal";
import { Redirect } from "react-router-dom";
import useModal from "../../../hooks/useModal";
import CreateSubjectModal from "../../../modals/CreateSubjectModal";
import {NavLink} from "react-router-dom";
import Subject from "../../../components/Subject";
import {useUserContext} from "../../../context/UserContext";
import { useParams } from "react-router";

function SubjectsList({}) {
    const {user} = useUserContext();
    const [classroomsId] = useState(parseInt(useParams().idClassroom));
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomsId));
    const { open, openModal, closeModal } = useModal();

    if(classroomIndex === -1) {
        return <Redirect to="/dashboard/classes"/>
    }

    const [editable] = useState((user.classrooms[classroomIndex].admin_id === user.id || user.classrooms[classroomIndex].user_id === user.id));

    return (
        (user.classrooms[classroomIndex] && (
        <section id="courses" className="container classroom">
            <div className="row">
                <div className="col-12">
                    <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes'}>
                        Toutes les classes
                    </NavLink>
                    <div className="d-flex justify-content-between align-items-center">
                        <h1 className="dark mb-4 mt-3">
                            { user.classrooms[classroomIndex].name }
                        </h1>
                    </div>
                </div>

                {editable && (
                        <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                            <div className="new-school-subject" onClick={openModal}>
                                <i className="fas fa-plus"></i>
                            </div>
                        </div>
                )}

                {editable && (!user.classrooms[classroomIndex].subjects || user.classrooms[classroomIndex].subjects.length === 0) && (
                    <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                        <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                        <div className="grey">Commencez par créer une matière.</div>
                    </div>
                )}

                {user.classrooms[classroomIndex].subjects && (
                    Object.values(user.classrooms[classroomIndex].subjects).map((subject) => {
                        return (
                            <div className="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12" key={subject.id}>
                                <NavLink to={'/dashboard/classes/' + classroomsId + '/sujet/' + subject.id} className="text-decoration-none">
                                    <Subject subject={subject}/>
                                </NavLink>
                            </div>
                        )
                    })
                )}

                <Modal open={open} close={closeModal}>
                    <CreateSubjectModal classroomIndex={classroomIndex} close={closeModal}/>
                </Modal>

            </div>
        </section>
        ) ||
        <Redirect to="/dashboard/classes"/>
        )
    );
}

export default SubjectsList;
