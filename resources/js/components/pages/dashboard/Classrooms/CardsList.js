import React, {useState, useEffect} from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import DeleteSubChapterModal from "../../../modals/DeleteSubChapterModal";
import EditSubChapterModal from "../../../modals/EditSubChapterModal";
import {NavLink} from "react-router-dom";
import {useUserContext} from "../../../context/UserContext";
import EditButton from "../../../components/EditButton";
import Card from "../../../components/Card";
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import { reorder } from "../../../services/Helper";
import Api from "../../../services/Api";

function CardsList({}) {
    const {user, setUser} = useUserContext();
    const [classroomId] = useState(parseInt(useParams().idClassroom));
    const [subjectId] = useState(parseInt(useParams().idSubject));
    const [chapterId] = useState(parseInt(useParams().idChapter));
    const [subChapterId] = useState(parseInt(useParams().idSubChapter));
    const [cards, setCards] = useState([]);
    const { open, openModal, closeModal } = useModal();
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const { open: openEdit, openModal: openEditModal, closeModal: closeEditModal } = useModal();
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomId));

    if(!user.classrooms[classroomIndex]) {
        return <Redirect to="/dashboard/classes"/>
    }

    const [subjectIndex] = useState(user.classrooms[classroomIndex].subjects.findIndex(x => x.id === subjectId));

    if(!user.classrooms[classroomIndex].subjects[subjectIndex]) {
        return <Redirect to={'/dashboard/classes/' + classroomId}/>
    }

    const [chapterIndex] = useState(user.classrooms[classroomIndex].subjects[subjectIndex].chapters.findIndex(x => x.id === chapterId));

    if(!user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex]) {
        return <Redirect to={'/dashboard/classes/' + classroomId + '/sujet/' + subjectId}/>
    }

    const [subChapterIndex] = useState(user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters.findIndex(x => x.id === subChapterId));

    if(!user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex]) {
        return <Redirect to={'/dashboard/classes/' + classroomId + '/sujet/' + subjectId + '/chapitre/' + chapterId}/>
    }

    const [editable] = useState((user.classrooms[classroomIndex].admin_id === user.id || user.classrooms[classroomIndex].user_id === user.id));

    useEffect( () => {
        updateCards();
    },[user]);

    function updateCards() {
        let newData = [];

        Object.entries(user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards).map(([key, card]) => {
            newData.push(
                <Draggable key={card.order} draggableId={card.id.toString()} index={parseInt(key)} isDragDisabled={!editable}>
                    {(provided, snapshot) => (
                        <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                        >
                            <div className="col-12 d-flex align-items-center mb-3" key={key}>
                                <Card classroomIndex={classroomIndex}
                                      subjectIndex={subjectIndex}
                                      chapterIndex={chapterIndex}
                                      subChapterIndex={subChapterIndex}
                                      card={card}
                                      editable={editable}/>
                            </div>
                        </div>
                    )}
                </Draggable>
            );
        });

        setCards(newData);
    }

    function addCard() {
        let newData = [...cards];
        newData.unshift(
            <div className="col-12 d-flex align-items-center mb-3" key={newData.length}>
                <Card classroomIndex={classroomIndex}
                      subjectIndex={subjectIndex}
                      chapterIndex={chapterIndex}
                      subChapterIndex={subChapterIndex}
                      edit={true}
                      editable={editable}/>
            </div>
            );
        setCards(newData);
    }

    function onDragEnd(result) {
        if (!result.destination) {
            return;
        }

        const items = reorder(
            user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards,
            result.source.index,
            result.destination.index
        );

        console.log(items);

        let newUser = {...user};
        newUser.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards = items;
        setUser(newUser);

        updateCards();

        Api.updateCardOrder(result.draggableId, {order: result.destination.index}).then(response => {
            console.log(response);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        <section id="courses" className="container">
            <div className="row">
                <div className="col-12">
                    <div>
                        <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes'}>
                            Toutes les classes
                        </NavLink>
                        <span className="grey text-uppercase font-size-12"> - </span>
                        <NavLink className="grey text-uppercase font-size-12"
                                 to={'/dashboard/classes/' + classroomId}>
                            {user.classrooms[classroomIndex].name}
                        </NavLink>
                        <span className="grey text-uppercase font-size-12"> - </span>
                        <NavLink className="grey text-uppercase font-size-12"
                                 to={'/dashboard/classes/' + classroomId + '/sujet/' + subjectId}>
                            {user.classrooms[classroomIndex].subjects[subjectIndex].name}
                        </NavLink>
                        <span className="grey text-uppercase font-size-12"> - </span>
                        <NavLink className="grey text-uppercase font-size-12"
                                 to={'/dashboard/classes/' + classroomId + '/sujet/' + subjectId + '/chapitre/' + chapterId}>
                            {user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].name}
                        </NavLink>
                    </div>
                    <div className="d-flex justify-content-between align-items-center">
                        <h1 className="dark mb-4 mt-3">
                            {user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].name}
                        </h1>

                        {editable && (
                            <EditButton edit={openEditModal} destroy={openDeleteModal}/>
                        )}
                    </div>
                </div>

                {editable && (
                    <div className="col-12 d-flex align-items-center mb-5">
                        <div className="add-button-cards" onClick={addCard}><i className="fas fa-plus"></i></div>

                        {user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.length === 0 && (
                            <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                                <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                                <div className="grey">Ajouter une carte !</div>
                            </div>
                        )}
                    </div>
                ) || (
                    <div className="grey">
                        {!user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards ||
                        user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].cards.length === 0 && (
                            <span>Aucune carte disponible.</span>
                        )}
                    </div>
                )}

                <div className="col-12">
                    <DragDropContext onDragEnd={onDragEnd}>
                        <Droppable droppableId="droppable">
                            {(provided, snapshot) => (
                                <div
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    {cards}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>

                <Modal open={openDelete} close={closeDeleteModal}>
                    <DeleteSubChapterModal classroomIndex={classroomIndex} subjectIndex={subjectIndex} chapterIndex={chapterIndex} subChapterIndex={subChapterIndex}/>
                </Modal>

                <Modal open={openEdit} close={closeEditModal}>
                    <EditSubChapterModal classroomIndex={classroomIndex} subjectIndex={subjectIndex} chapterIndex={chapterIndex} subChapterIndex={subChapterIndex} closeModal={closeEditModal}/>
                </Modal>

            </div>
        </section>
    );
}

export default CardsList;
