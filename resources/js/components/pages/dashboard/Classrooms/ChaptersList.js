import React, {useEffect, useState} from 'react';
import Modal from "../../../components/Modal";
import useModal from "../../../hooks/useModal";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import CreateChapterModal from "../../../modals/CreateChapterModal";
import DeleteSubjectModal from "../../../modals/DeleteSubjectModal";
import EditSubjectModal from "../../../modals/EditSubjectModal";
import {NavLink} from "react-router-dom";
import Chapter from "../../../components/Chapter";
import {useUserContext} from "../../../context/UserContext";
import EditButton from "../../../components/EditButton";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { reorder } from "../../../services/Helper";
import Api from "../../../services/Api";

function ChaptersList({}) {
    const {user, setUser} = useUserContext();
    const [classroomsId] = useState(parseInt(useParams().idClassroom));
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomsId));
    if(classroomIndex === -1) {
        return <Redirect to="/dashboard/classes"/>
    }
    const [subjectId] = useState(parseInt(useParams().idSubject));
    const [subjectIndex] = useState(user.classrooms[classroomIndex].subjects.findIndex(x => x.id === subjectId));
    const { open, openModal, closeModal } = useModal();
    const { open: openDelete, openModal: openDeleteModal, closeModal: closeDeleteModal } = useModal();
    const { open: openEdit, openModal: openEditModal, closeModal: closeEditModal } = useModal();

    const [editable] = useState((user.classrooms[classroomIndex].admin_id === user.id || user.classrooms[classroomIndex].user_id === user.id));

    function onDragEnd(result) {
        if (!result.destination) {
            return;
        }

        const items = reorder(
            user.classrooms[classroomIndex].subjects[subjectIndex].chapters,
            result.source.index,
            result.destination.index
        );

        let newUser = {...user};
        newUser.classrooms[classroomIndex].subjects[subjectIndex].chapters = items;
        setUser(newUser);

        Api.updateChapterOrder(result.draggableId, {order: result.destination.index}).then(response => {
            console.log(response);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        (user.classrooms[classroomIndex].subjects[subjectIndex] && (
            <section id="courses" className="container classroom">
                <div className="row">
                    <div className="col-12">
                        <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes'}>
                            Toutes les classes
                        </NavLink>
                        <span className="grey text-uppercase font-size-12"> - </span>
                        <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes/' + classroomsId}>
                            { user.classrooms[classroomIndex].name }
                        </NavLink>
                        <div className="d-flex justify-content-between align-items-center">
                            <h1 className="dark mb-4 mt-3">
                                { user.classrooms[classroomIndex].subjects[subjectIndex].name }
                            </h1>

                            {editable && (
                                <EditButton edit={openEditModal} destroy={openDeleteModal}/>
                            )}
                        </div>
                    </div>

                    {editable && (
                        <div className="col-12 d-flex align-items-center mb-5">
                            <div className="add-button-chapters" onClick={openModal}><i className="fas fa-plus"></i></div>

                            {!user.classrooms[classroomIndex].subjects[subjectIndex].chapters ||
                                user.classrooms[classroomIndex].subjects[subjectIndex].chapters.length === 0 && (
                                <div className="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 d-flex align-items-center">
                                    <div className="ml-5 mr-4 grey"><i className="fas fa-arrow-left"></i></div>
                                    <div className="grey">Commencez par créer un chapitre.</div>
                                </div>
                            )}
                        </div>
                    ) || (
                        <div className="grey">
                            {!user.classrooms[classroomIndex].subjects[subjectIndex].chapters ||
                            user.classrooms[classroomIndex].subjects[subjectIndex].chapters.length === 0 && (
                                <span>Aucun chapitre disponible.</span>
                            )}
                        </div>
                    )}

                    {user.classrooms[classroomIndex].subjects[subjectIndex].chapters.length !== 0 && (
                        <div className="col-12">
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="droppable">
                                    {(provided, snapshot) => (
                                        <div
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                        >
                                            {Object.entries(user.classrooms[classroomIndex].subjects[subjectIndex].chapters).map(([key, chapter]) => (
                                                <Draggable key={chapter.id} draggableId={chapter.id.toString()} index={parseInt(key)} isDragDisabled={!editable}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <NavLink to={'/dashboard/classes/' + classroomsId + '/sujet/' + subjectId + '/chapitre/' + chapter.id} key={chapter.id}
                                                                     className="text-decoration-none">
                                                                <Chapter chapter={chapter}/>
                                                            </NavLink>
                                                        </div>
                                                    )}
                                                </Draggable>
                                            ))}
                                            {provided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                            </DragDropContext>
                        </div>
                    )}

                    {editable && (
                        <div>
                            <Modal open={open} close={closeModal}>
                                <CreateChapterModal classroomIndex={classroomIndex} subjectIndex={subjectIndex} close={closeModal}/>
                            </Modal>

                            <Modal open={openDelete} close={closeDeleteModal}>
                                <DeleteSubjectModal classroomIndex={classroomIndex} subjectIndex={subjectIndex}/>
                            </Modal>

                            <Modal open={openEdit} close={closeEditModal}>
                                <EditSubjectModal classroomIndex={classroomIndex} subjectIndex={subjectIndex} closeModal={closeEditModal}/>
                            </Modal>
                        </div>
                    )}
                </div>
            </section>
        ) ||
            <Redirect to={'/dashboard/classes/' + classroomsId} />
        )
    );
}

export default ChaptersList;
