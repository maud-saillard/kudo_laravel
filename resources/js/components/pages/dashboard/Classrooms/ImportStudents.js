import React, {useState} from 'react';
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import {useUserContext} from "../../../context/UserContext";
import {NavLink} from "react-router-dom";
import useForm from "../../../hooks/useForm";
import SubmitButton from "../../../components/SubmitButton";
import Api from "../../../services/Api";

function ImportStudents({}) {
    const {user, setUser} = useUserContext();
    const [classroomId] = useState(parseInt(useParams().id));
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomId));
    const [redirect, setRedirect] = useState(false);

    const initialState = {
        students: '',
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const submit = (e) => {
        setOnLoad(true);

        Api.addStudents(classroomId, form).then(response => {
            console.log(response.data);
            let newUser = {...user};
            newUser.classrooms[classroomIndex] = response.data.data;
            setUser(newUser);
            setRedirect(true);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    if(redirect) {
        return <Redirect to={'/dashboard/classes/manage/' + classroomId + '/students'}/>
    }

    return (
        (user.classrooms[classroomIndex] && (
                <section id="classroom" className="container">
                    <div className="row">
                        <div className="col-12">
                            <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes/manage/' + classroomId + '/students'}>
                                { user.classrooms[classroomIndex].name }
                            </NavLink>
                            <div className="d-flex justify-content-between align-items-center">
                                <h1 className="dark mb-4 mt-3">
                                    Étudiants de la classe
                                </h1>
                            </div>
                        </div>

                        <div className="col-12">
                            <div className="block-white">
                                <div className="d-flex">
                                    <SubmitButton submit={() => submit()} onLoad={onLoad} disabled={!(!!form.students)} title={'Importer'}/>
                                    <NavLink className="btn-green btn-smaller d-flex flex-1 justify-content-center" to={'/dashboard/classes/manage/' + classroomId + '/students'}>
                                        Annuler
                                    </NavLink>
                                </div>

                                <p className="grey mt-3">Importez vos données, copiez et collez votre liste ici (à partir de fichiers Word, Excel…) </p>

                                <div className="settings-form-group no-border">
                                    <textarea className="form-control p-4" name="students" onChange={setForm} rows={16} value={form.students} placeholder={'maildeletudiant@gmail.com'}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            ) ||
            <Redirect to="/dashboard/classes"/>
        )
    );
}

export default ImportStudents;
