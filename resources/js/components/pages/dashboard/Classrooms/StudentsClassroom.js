import React, {useState} from 'react';
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import {useUserContext} from "../../../context/UserContext";
import {NavLink} from "react-router-dom";
import useForm from "../../../hooks/useForm";
import Api from "../../../services/Api";
import Chapter from "../../../components/Chapter";
import CreateChapterModal from "../../../modals/CreateChapterModal";
import Modal from "../../../components/Modal";
import AddStudentModal from "../../../modals/AddStudentModal";
import useModal from "../../../hooks/useModal";

function StudentsClassroom({}) {
    const {user, setUser} = useUserContext();
    const [classroomId] = useState(parseInt(useParams().id));
    const [classroomIndex] = useState(user.classrooms.findIndex(x => x.id === classroomId));
    const { open, openModal, closeModal } = useModal();

    const initialState = {
        admin_name: '',
        admin_email: '',
    };
    const [form, setForm] = useForm(initialState);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const submit = (e) => {
        setOnLoad(true);

        Api.updateClassroomAdmin(classroomId, form).then(response => {
            setForm(initialState);
            let newUser = {...user};
            newUser.classrooms[classroomIndex] = response.data.data;
            setUser(newUser);
            setSuccess(true);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    function deleteStudent(idStudent) {
        Api.removeStudent(classroomId, idStudent).then(response => {
            console.log(response.data);
            let newUser = {...user};
            newUser.classrooms[classroomIndex] = response.data.data;
            setUser(newUser);
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        (user.classrooms[classroomIndex] && (
                <section id="classroom" className="container">
                    <div className="row">
                        <div className="col-12">
                            <NavLink className="grey text-uppercase font-size-12" to={'/dashboard/classes/manage/' + classroomId}>
                                { user.classrooms[classroomIndex].name }
                            </NavLink>
                            <div className="d-flex justify-content-between align-items-center">
                                <h1 className="dark mb-4 mt-3">
                                    Étudiants de la classe
                                </h1>
                            </div>
                        </div>

                        <div className="col-6">
                            <div className="block-white p-2 pt-4">
                                <div className="col-12 d-flex align-items-center justify-content-between mb-5">
                                    <div className="add-button-chapters" onClick={openModal}><i className="fas fa-plus"></i></div>
                                    <NavLink className="btn-green btn-smaller flex-1"
                                            to={'/dashboard/classes/manage/' + classroomId + '/import'}>
                                        IMPORTER une liste
                                    </NavLink>
                                </div>

                                <div className="col-12">
                                    {user.classrooms[classroomIndex].students && (
                                        Object.values(user.classrooms[classroomIndex].students).map((student) => {
                                            return (
                                                <div className={'email-student'} key={student.id}>
                                                    <span className={'email'}>{ student.email }</span>
                                                    <span onClick={() => deleteStudent(student.id)}><i className="fas fa-trash"></i></span>
                                                </div>
                                            )
                                        })
                                    )}
                                </div>

                            </div>
                        </div>

                        <div className="col-6 p-2">
                            <div className="block-white">
                                <div className="title mb-3">Liste des étudiants</div>

                                <p className="grey font-size-15 mb-3"><b>
                                    { user.classrooms[classroomIndex].students.length } </b>
                                    étudiant{ user.classrooms[classroomIndex].students.length > 1 ? 's' : '' } inscrit{ user.classrooms[classroomIndex].students.length > 1 ? 's' : '' }
                                </p>
                            </div>
                        </div>

                        <Modal open={open} close={closeModal}>
                            <AddStudentModal classroomIndex={classroomIndex} close={closeModal}/>
                        </Modal>
                    </div>
                </section>
            ) ||
            <Redirect to="/dashboard/classes"/>
        )
    );
}

export default StudentsClassroom;
