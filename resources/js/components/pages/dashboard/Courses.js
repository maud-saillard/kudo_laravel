import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import ChaptersList from "./Courses/ChaptersList";
import SubjectsList from "./Courses/SubjectsList";
import SubChaptersList from "./Courses/SubChaptersList";
import CardsList from "./Courses/CardsList";
import {useUserContext} from "../../context/UserContext";

function Courses() {
    const {user} = useUserContext();

    if(user.tutor) {
        return <Redirect to="/dashboard/classes"/>
    }

    return (
        <Switch>
            <Route path="/dashboard/mes-cours/:idSubject/chapitre/:idChapter/sous-chapitre/:idSubChapter">
                <CardsList/>
            </Route>
            <Route path="/dashboard/mes-cours/:idSubject/chapitre/:idChapter">
                <SubChaptersList/>
            </Route>
            <Route path="/dashboard/mes-cours/:id">
                <ChaptersList/>
            </Route>
            <Route path={'/dashboard/mes-cours'}>
                <SubjectsList subjects={user.subjects}/>
            </Route>
        </Switch>
    );
}

export default Courses;
