import React from 'react';
import ContactForm from '../../forms/ContactForm';

function Contact() {
    return (
        <section id="contact-form">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-12 d-flex flex-column justify-content-center mb-4">
                        <h2 className="mb-4">Contactez-nous !</h2>
                        <p className={'grey'}>
                            Nos équipes seront ravies de pouvoir vous aider.
                        </p>
                    </div>
                    <div className="col-lg-12 col-md-12 col-12">
                        <ContactForm/>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Contact;
