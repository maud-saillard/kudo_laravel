import React from 'react';
import {Switch, Route} from "react-router-dom";
import ClassroomsList from "./Classrooms/ClassroomsList";
import ManageClassroom from "./Classrooms/ManageClassroom";
import {useUserContext} from "../../context/UserContext";
import StudentsClassroom from "./Classrooms/StudentsClassroom";
import ImportStudents from "./Classrooms/ImportStudents";
import SubjectsList from "./Classrooms/SubjectsList";
import ChaptersList from "./Classrooms/ChaptersList";
import SubChaptersList from "./Classrooms/SubChaptersList";
import CardsList from "./Classrooms/CardsList";

function Classrooms() {
    const {user} = useUserContext();

    return (
        <Switch>
            <Route path={'/dashboard/classes/manage/:id/import'}>
                <ImportStudents/>
            </Route>
            <Route path={'/dashboard/classes/manage/:id/students'}>
                <StudentsClassroom/>
            </Route>
            <Route path={'/dashboard/classes/manage/:id'}>
                <ManageClassroom/>
            </Route>

            <Route path="/dashboard/classes/:idClassroom/sujet/:idSubject/chapitre/:idChapter/sous-chapitre/:idSubChapter">
                <CardsList/>
            </Route>
            <Route path="/dashboard/classes/:idClassroom/sujet/:idSubject/chapitre/:idChapter">
                <SubChaptersList/>
            </Route>
            <Route path="/dashboard/classes/:idClassroom/sujet/:idSubject">
                <ChaptersList/>
            </Route>
            <Route path="/dashboard/classes/:idClassroom">
                <SubjectsList/>
            </Route>

            <Route path={['/dashboard/classes']}>
                <ClassroomsList classrooms={user.classrooms}/>
            </Route>
        </Switch>
    );
}

export default Classrooms;
