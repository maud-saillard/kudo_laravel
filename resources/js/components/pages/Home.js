import React from 'react';

function Home() {
    return (
        <div>
            <section id="homepage">
                <div className="container h-100">
                    <div className="row h-100 reverse-mobile">
                        <div className="col-lg-6 col-md-12 col-12 text-right h-100">
                            <img src="/images/01_illu_home.png" className="illustration"/>
                        </div>
                        <div className="col-lg-6 col-md-12 col-12 py-4">
                            <div className="logo"><img src={'/images/logo.svg'} alt={'Kudo'} /></div>
                            <h2 className="black mb-4">L’apprentissage qui ne sort<br/> plus de la tête !</h2>
                            <p className="grey">Kudo a été développé pour vous aider à mémoriser vos cours sur le long terme.</p>

                            <div className="text-center">
                                <a href="#" className="btn-green-full">Téléchargez l’application</a>
                                <a href="#contact" className="btn-green">Contactez-nous</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="homepage-presentation-student">
                <div className="container h-100">
                    <div className="row h-100">
                        <div className="col-12 grey text-uppercase text-center mb-5">Vous êtes un étudiant</div>
                    </div>

                    <div className="row h-100 reverse-mobile">
                        <div className="col-lg-6 col-md-12 col-12 text-right h-100 mb-5">
                            <img src="/images/02_illu_home.png" className="img-fluid"/>
                        </div>
                        <div className="col-lg-6 col-md-12 col-12 py-4 d-flex flex-column justify-content-center mb-5">
                            <h2 className="black mb-4">Révisez au bon moment <br/> et optimisez votre mémoire</h2>
                            <p className="grey">L’apprentissage par répétition espacée vous permettra de réviser au bon moment pour optimiser votre mémoire à long terme.</p>
                        </div>
                    </div>

                    <div className="row h-100">
                        <div className="col-lg-6 col-md-12 col-12 text-left h-100">
                            <div>
                                <h2 className="black mb-4">
                                    Rendez vos révisions <br/> plus agréables</h2>
                                <p className="grey">
                                    Les cartes mémoires vous permettent de vous
                                    auto-interroger et vérifier par vous-même l’état
                                    d’avancement de votre apprentissage.
                                </p>
                            </div>
                            <div className="text-center mt-80">
                                <img src="/images/04_illu_home.png" className="img-fluid"/>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-12 col-12 d-flex flex-column justify-content-center reverse-mobile">
                            <div className="text-center mb-5">
                                <img src="/images/03_illu_home.png" className="img-fluid"/>
                            </div>
                            <div>
                                <h2 className="black mt-5 mb-4">Gagnez du temps dans vos révisions et suivez votre apprentissage</h2>
                                <p className="grey">
                                    Soyez actif et performant dans votre apprentissage en insistant sur les notions moins connues. Votre tableau de bord vous indiquera l’évolution de vos apprentissages au jour le jour.
                                </p>
                                <div className="text-center mt-80">
                                    <a href="#" className="btn-green-full">Téléchargez l’application</a>
                                    <a href="#contact" className="btn-green">Contactez-nous</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="homepage-presentation-tutorat">
                <div className="container h-100">
                    <div className="row h-100">
                        <div className="col-12 grey text-uppercase text-center mb-100">Vous êtes Une université ou un tutorat ?</div>
                    </div>

                    <div className="row h-100 mb-100">
                        <div className="col-lg-6 col-md-6 col-12 py-4 d-flex flex-column justify-content-center">
                            <h2 className="black">Créez vos classes pour réviser<br/>à plusieurs</h2>
                            <p className="grey">
                                Créez une classe et donnez-en l’accès à votre groupe
                                de travail grâce à un lien sécurisé envoyé par mail.
                            </p>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12 text-right h-100">
                            <img src="/images/05_illu_home.png" className="img-fluid"/>
                        </div>
                    </div>

                    <div className="row h-100 mb-100">
                        <div className="col-lg-6 col-md-6 col-12 text-right h-100">
                            <img src="/images/06_illu_home.png" className="img-fluid"/>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12 py-4 d-flex flex-column justify-content-center">
                            <h2 className="black mb-4">Permettez à chacun d’ajouter des cours tout en gardant le contrôle.</h2>
                            <p className="grey">
                                Donnez des accès à durée limitée aux membres de votre groupe
                                pour ajouter des cours. Votre rôle d’administrateur vous donne
                                la possibilité de garder la main.
                            </p>
                        </div>
                    </div>

                    <div className="row h-100">
                        <div className="col-lg-6 col-md-6 col-12 py-4 d-flex flex-column justify-content-center mb-5">
                            <h2 className="black mb-4">Motivez-vous en équipe<br/>et gagnez du temps</h2>
                            <p className="grey">
                                Les classes sont un gain de temps. Co-construisez
                                vos révisions avec vos camarades et challengez-vous
                                sur les connaissances à avoir.
                            </p>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12 text-center h-100 mb-5">
                            <img src="/images/07_illu_home.png" className="img-fluid"/>
                        </div>
                    </div>

                    <div className="row h-100">
                        <div className="col-12 grey text-uppercase text-center mb-80">Les universités déjà partenaires</div>
                    </div>

                    <div className="row h-100 partenaires">
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                        <div className="col-lg-3 col-md-3 col-6"><div><img src="/images/07_illu_home.png" className="img-fluid"/></div></div>
                    </div>

                    <div className="row h-100 mt-5">
                        <div className="col-12">
                            <div className="mb-4">
                                <p className="text-center grey mx-auto">«  Lorem Ipsum avis utilisateur Lorem Ipsum avis utilisateur sur deux lignes ou trois lignes. m avis utilisateur sur deux lignes ou trois lignes. »</p>
                            </div>

                            <div className="text-center black">
                                <b>prénom NOM</b><br/>
                                <span>Profession/université</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Home;
