import React from 'react';
import {Link, useLocation} from "react-router-dom";
import LoginForm from './../forms/LoginForm';
import RegisterForm from './../forms/RegisterForm';
import ResetPasswordForm from "../forms/ResetPasswordForm";

function Login() {
    return (
        <section id="login">
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-lg-6 col-md-12 col-12 h-100">

                        <b className="light-grey text-uppercase">Vous êtes</b>
                        <div className="black font-size-34">un <span className="green underline">étudiant</span> ?</div>
                        <div className="black font-size-34">une association ou un tutorat ?</div>

                        <div className="mt-4 mb-3">
                            <Link className={'grey mr-4'  + (useLocation().pathname === '/login' ? ' active-grey-link' : '')} to={'/login'}>Connexion</Link>
                            <Link className={'grey' + (useLocation().pathname === '/register' ? ' active-grey-link' : '')} to={'/register'}>Je n'ai pas de compte</Link>
                        </div>

                        {(useLocation().pathname === '/login') &&
                        <LoginForm/>
                        || (useLocation().pathname === '/login') &&
                        <ResetPasswordForm/>
                        ||
                        <RegisterForm/>
                        }

                        <div className="row">
                            <div className="col-6 text-left mt-4 grey small">
                                <div className="mb-4">Ou connectez-vous via</div>
                                <div className="d-flex mb-4 wrap-mobile">
                                    <a href="" className="social-network-black mr-3"><img src={'/images/facebook-black.svg'} alt={'Faceook'}/></a>
                                    <a href="" className="social-network-black mr-3"><img src={'/images/google-plus-black.svg'} alt={'Instagram'}/></a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-lg-6 col-md-12 col-12 py-4"></div>
                </div>
            </div>
        </section>
    );
}

export default Login;
