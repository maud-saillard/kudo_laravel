import React from 'react';
import ResetPasswordForm from "../forms/ResetPasswordForm";

function ResetPassword() {
    return (
        <section id="login">
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-lg-12 col-md-12 col-12 h-100">
                        <b className="light-grey text-uppercase">Mot de passe oublié ?</b>
                        <div className="black font-size-34">Réinitialisez votre mot de passe </div>
                    </div>

                    <div className="col-lg-6 col-md-12 col-12 h-100">
                        <ResetPasswordForm/>
                    </div>
                    <div className="col-lg-6 col-md-12 col-12 py-4"></div>
                </div>
            </div>
        </section>
    );
}

export default ResetPassword;
