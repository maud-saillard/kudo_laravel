import React, {useState} from 'react';
import { useParams } from "react-router";
import NewPasswordForm from "../forms/NewPasswordForm";

function NewPassword() {
    return (
        <section id="login">
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-lg-12 col-md-12 col-12 h-100">
                        <b className="light-grey text-uppercase">Réinitialisation de votre mot de passe</b>
                        <div className="black font-size-34">Réinitialiser le mot de passe de <br/> { useParams().email }</div>
                    </div>

                    <div className="col-lg-6 col-md-12 col-12 h-100 mt-3">
                        <NewPasswordForm token={useParams().token} email={useParams().email}/>
                    </div>
                    <div className="col-lg-6 col-md-12 col-12 py-4"></div>
                </div>
            </div>
        </section>
    );
}

export default NewPassword;
