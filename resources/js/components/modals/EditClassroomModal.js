import React, {useState} from 'react';
import {useUserContext} from "../context/UserContext";
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import useForm from "../hooks/useForm";

function EditClassroomModal({classroomIndex, closeModal}) {
    const {user, setUser} = useUserContext();
    const [form, setForm] = useForm(user.classrooms[classroomIndex]);
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);
    const [errors, setErrors] = useState([]);

    const submit = () => {
        setOnLoad(true);

        Api.updateClassroom(user.classrooms[classroomIndex].id, form).then(response => {
            let newUser = {...user};
            newUser.classrooms[classroomIndex] = response.data.data;
            setUser(newUser);
            setSuccess(true);
            closeModal();
        }).catch(error => {
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <div>
            <div className="form-group input-no-border">
                <input type="text" name="name" onChange={setForm} value={form.name} className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')} placeholder="Nom"/>
                { (errors && errors.name) && <span className="invalid-feedback" role="alert"> {errors.name[0] } </span> }
            </div>

            <div className="d-flex justify-content-center">
                <SubmitButton onLoad={onLoad} submit={() => submit()} disabled={(form.name === user.classrooms[classroomIndex].name)} title={'Renommer'}/>
            </div>
        </div>
    );
}

export default EditClassroomModal;
