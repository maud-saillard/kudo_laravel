import React, {useState, useEffect} from 'react';
import {useUserContext} from "../context/UserContext";
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { Redirect } from "react-router-dom";

function DeleteClassroomModal({classroomIndex}) {
    const {user, setUser} = useUserContext();
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);
    const [error, setError] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [newUser, setNewUser] = useState([]);

    useEffect( () => {
        if(success) {
            setUser(newUser);
            setRedirect(true);
        }
    }, [success]);

    const submit = () => {
        setOnLoad(true);

        Api.deleteClassroom(user.classrooms[classroomIndex].id).then(response => {
            let newData = {...user};
            newData.classrooms.splice(classroomIndex, 1);
            setNewUser(newData);
            setSuccess(true);
        }).catch(error => {
            if (error.response.status === 422) {
                setError(error.response.data.message || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    if(redirect) {
        return <Redirect to="/dashboard/classes"/>
    }

    return (
        <div>
            <div className="grey text-center mb-4">
                Souhaitez-vous supprimer définitivement cette classe ?<br/>
                Les étudiants et vous n’auront plus accès aux données de la classe.
            </div>

            { (error) && <div className="text-danger text-center mb-3"> { error } </div> }

            <div className="d-flex justify-content-center">
                <SubmitButton onLoad={onLoad} submit={() => submit()} disabled={false} title={'Supprimer'}/>
            </div>
        </div>
    );
}

export default DeleteClassroomModal;
