import React, {useState} from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { useHistory } from "react-router-dom";
import {useUserContext} from "../context/UserContext";

function CreateSubChapterModal({classroomIndex = null, subjectIndex, chapterIndex, close}) {
    const {user, setUser} = useUserContext();
    const history = useHistory();
    const [name, setName] = useState('');
    const [subChapter, setSubChapter] = useState(null);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setName('');
        setSuccess('');
        setErrors([]);
    };

    function submitValue() {
        if(name) {
            setOnLoad(true);

            const form = {
                name: name
            };

            let idChapter =  null;
            if(classroomIndex !== null) {
                idChapter = user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].id;
            } else {
                idChapter = user.subjects[subjectIndex].chapters[chapterIndex].id;
            }

            Api.postSubChapter(form, idChapter).then(response => {
                clearForm();
                setSubChapter(response.data.data);
                let newData = {...user};

                if(classroomIndex !== null) {
                    newData.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters.push(response.data.data);
                } else {
                    newData.subjects[subjectIndex].chapters[chapterIndex].subChapters.push(response.data.data);
                }

                setUser(newData);
                setSuccess(true);
                close();
            }).catch(error => {
                console.log(error);
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function newSubChapter() {
        setSubChapter(null);
    }

    function newCard() {
        if(classroomIndex !== null) {
            history.push('/dashboard/classes/' + user.classrooms[classroomIndex].id +'/sujet/' + user.classrooms[classroomIndex].subjects[subjectIndex].id  + '/chapitre/' + user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].id + '/sous-chapitre/' + subChapter.id);
        } else {
            history.push('/dashboard/mes-cours/' + user.subjects[subjectIndex].id + '/chapitre/' + user.subjects[subjectIndex].chapters[chapterIndex].id + '/sous-chapitre/' + subChapter.id);
        }
    }

    return (
        (subChapter &&
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Nouveau sous chapitre</div>

                    <div className="black font-weight-bold text-center">
                        { subChapter.name }
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="row">
                        <div className="col-6">
                            <button type="submit" className="btn-green-full btn-smaller" onClick={newCard}>
                                Créer une carte
                            </button>
                        </div>
                        <div className="col-6">
                            <button type="submit" className="btn-green btn-smaller" onClick={newSubChapter}>
                                Créer un sous chapitre
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        ||
        <div>
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Créer un sous chapitre</div>

                    <div className="form-group input-no-border">
                        <input type="text" name="name" onChange={e => setName(e.target.value)} value={name}
                               className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                               placeholder="Nom"/>
                        {(errors && errors.name) &&
                        <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="col-6 mx-auto">
                        <SubmitButton onLoad={onLoad} submit={() => submitValue()} disabled={!(!!name)}/>
                    </div>
                </div>
            </div>
        </div>
        )
    );
}

export default CreateSubChapterModal;
