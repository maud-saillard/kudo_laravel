import React, {useState} from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { useHistory } from "react-router-dom";
import {useUserContext} from "../context/UserContext";
import useForm from "../hooks/useForm";

function AddStudentModal({classroomIndex, close}) {
    const history = useHistory();
    const {user, setUser} = useUserContext();
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);

    const initialState = {students: '',};
    const [form, setForm] = useForm(initialState);

    const clearForm = () => {
        setForm(initialState);
        setSuccess(false);
        setErrors([]);
    };

    function submitValue() {
        if(form.students) {
            setOnLoad(true);
            Api.addStudents(user.classrooms[classroomIndex].id, form).then(response => {
                let newUser = {...user};
                newUser.classrooms[classroomIndex] = response.data.data;
                setUser(newUser);
                clearForm();
                close();
            }).catch(error => {
                console.log(error);
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function newSubject() {
        setSubject(null);
    }

    function newChapter() {
        history.push('/dashboard/mes-cours/' + subject.id);
    }

    return (
        <div>
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Ajouter un étudiant</div>

                    <div className="form-group input-no-border">
                        <input type="text" name="students" onChange={setForm} value={form.students}
                               className={'form-control' + ((errors && errors.students) ? ' is-invalid' : '')}
                               placeholder="Email"/>
                        {(errors && errors.students) &&
                        <span className="invalid-feedback" role="alert"> {errors.students[0]} </span>}
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="col-6 mx-auto">
                        <SubmitButton title={'Ajouter'} onLoad={onLoad} submit={() => submitValue()} disabled={!(!!form.students)}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AddStudentModal;
