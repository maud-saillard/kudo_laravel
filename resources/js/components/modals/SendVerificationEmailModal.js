import React from 'react';
import { useState } from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";

function SendVerificationEmailModal(props) {
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);

    const submit = (e) => {
        setOnLoad(true);

        Api.sendVerificationEmail().then(response => {
            setOnLoad(false);
            localStorage.removeItem('token');
            window.location.replace('/login');
        }).catch(error => {
            console.log(error);
            if (error.response.status === 422) {

            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <div>
            <p className="grey text-center mt-4 font-size-12">Vous devez vérifier votre email avant de pouvoir continuer</p>
            <div className="d-flex justify-content-center">
                <SubmitButton title={'Renvoyer un email'} onLoad={onLoad} submit={() => submit()} className="mx-auto"/>
            </div>
        </div>
    );
}

export default SendVerificationEmailModal;
