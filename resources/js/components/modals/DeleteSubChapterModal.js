import React, {useState, useEffect} from 'react';
import {useUserContext} from "../context/UserContext";
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { Redirect } from "react-router-dom";

function DeleteSubChapterModal({classroomIndex = null, subjectIndex, chapterIndex, subChapterIndex}) {
    const {user, setUser} = useUserContext();
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);
    const [error, setError] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [newUser, setNewUser] = useState([]);

    useEffect( () => {
        if(success) {
            setRedirect(true);
        }
    }, [success]);

    const submit = () => {
        setOnLoad(true);

        let idSubChapter =  null;
        if(classroomIndex !== null) {
            idSubChapter = user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].id;
        } else {
            idSubChapter = user.subjects[subjectIndex].chapters[chapterIndex].subChapters[subChapterIndex].id;
        }

        Api.deleteSubChapter(idSubChapter).then(response => {
            let newData = {...user};

            if(classroomIndex !== null) {
                newData.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].subChapters.splice(subChapterIndex, 1);
            } else {
                newData.subjects[subjectIndex].chapters[chapterIndex].subChapters.splice(subChapterIndex, 1);
            }
            setNewUser(newData);
            setSuccess(true);
        }).catch(error => {
            if (error.response.status === 422) {
                setError(error.response.data.message || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    if(redirect) {
        if(classroomIndex !== null) {
            return <Redirect to={'/dashboard/classes/' + user.classrooms[classroomIndex].id + '/sujet/' + user.classrooms[classroomIndex].subjects[subjectIndex].id + '/chapitre/' + user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].id}/>
        } else {
            return <Redirect to={'/dashboard/mes-cours/' + user.subjects[subjectIndex].id + '/chapitre/' + user.subjects[subjectIndex].chapters[chapterIndex].id}/>
        }
    }

    return (
        <div>
            <div className="grey text-center mb-4">Etes vous sur de supprimer ce sous-chapitre ?</div>

            { (error) && <div className="text-danger text-center mb-3"> { error } </div> }

            <div className="d-flex justify-content-center">
                <SubmitButton onLoad={onLoad} submit={() => submit()} disabled={false} title={'Supprimer'}/>
            </div>
        </div>
    );
}

export default DeleteSubChapterModal;
