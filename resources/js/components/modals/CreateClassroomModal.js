import React, {useState} from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import {NavLink, useHistory} from "react-router-dom";
import {useUserContext} from "../context/UserContext";

function CreateClassroomModal({close}) {
    const history = useHistory();
    const {user, setUser} = useUserContext();
    const [name, setName] = useState('');
    const [classroom, setClassroom] = useState(null);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setName('');
        setSuccess(false);
        setErrors([]);
    };

    function submitValue() {
        if(name) {
            setOnLoad(true);

            const form = {
                name: name
            };

            Api.postClassroom(form).then(response => {
                clearForm();
                setClassroom(response.data.data);
                let newUser = {...user};
                newUser.classrooms.push(response.data.data);
                setUser(newUser);
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function newSubject() {
        setClassroom(null);
    }

    return (
        (classroom &&
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Nouvelle classe</div>

                    <div className="black font-weight-bold text-center">
                        { classroom.name }
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="row">
                        <div className="col-12 text-center">
                            <NavLink className="btn-green-full btn-smaller" to={'/dashboard/classes/manage/' + classroom.id}>
                                Accéder à la classe
                            </NavLink>
                        </div>
                    </div>
                </div>
            </div>
        ||
        <div>
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Créer une classe</div>

                    <div className="form-group input-no-border">
                        <input type="text" name="name" onChange={e => setName(e.target.value)} value={name}
                               className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                               placeholder="Nom"/>
                        {(errors && errors.name) &&
                        <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="col-6 mx-auto">
                        <SubmitButton onLoad={onLoad} submit={() => submitValue()} disabled={!(!!name)}/>
                    </div>
                </div>
            </div>
        </div>
        )

    );
}

export default CreateClassroomModal;
