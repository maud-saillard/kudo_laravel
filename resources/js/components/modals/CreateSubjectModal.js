import React, {useState} from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { useHistory } from "react-router-dom";
import {useUserContext} from "../context/UserContext";

function CreateSubjectModal({classroomIndex = null, close}) {
    const history = useHistory();
    const {user, setUser} = useUserContext();
    const [classroomId, setClassroomId] = useState((classroomIndex !== null) ? user.classrooms[classroomIndex].id : null);
    const [name, setName] = useState('');
    const [subject, setSubject] = useState(null);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setName('');
        setSuccess(false);
        setErrors([]);
    };

    function submitValue() {
        if(name) {
            setOnLoad(true);

            const form = {
                name: name,
                classroom_id: classroomId
            };

            Api.postSubject(form).then(response => {
                clearForm();
                setSubject(response.data.data);
                let newUser = {...user};
                if(classroomId) {
                    newUser.classrooms[classroomIndex].subjects.push(response.data.data);
                } else {
                    newUser.subjects.push(response.data.data);
                }
                setUser(newUser);
                close();
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function newSubject() {
        setSubject(null);
    }

    function newChapter() {
        if(classroomIndex !== null) {
            history.push('/dashboard/classes/'+ classroomId +'/sujet/' + subject.id);
        } else {
            history.push('/dashboard/mes-cours/' + subject.id);
        }
    }

    return (
        (subject &&
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Nouvelle matière</div>

                    <div className="black font-weight-bold text-center">
                        { subject.name }
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="row">
                        <div className="col-6">
                            <button type="submit" className="btn-green-full btn-smaller" onClick={newChapter}>
                                Créer un chapitre
                            </button>
                        </div>
                        <div className="col-6">
                            <button type="submit" className="btn-green btn-smaller" onClick={newSubject}>
                                Créer une autre matière
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        ||
        <div>
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Créer une matière</div>

                    <div className="form-group input-no-border">
                        <input type="text" name="name" onChange={e => setName(e.target.value)} value={name}
                               className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                               placeholder="Nom"/>
                        {(errors && errors.name) &&
                        <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="col-6 mx-auto">
                        <SubmitButton onLoad={onLoad} submit={() => submitValue()} disabled={!(!!name)}/>
                    </div>
                </div>
            </div>
        </div>
        )

    );
}

export default CreateSubjectModal;
