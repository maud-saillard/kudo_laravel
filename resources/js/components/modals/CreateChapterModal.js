import React, {useState} from 'react';
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { useHistory } from "react-router-dom";
import {useUserContext} from "../context/UserContext";

function CreateChapterModal({classroomIndex = null, subjectIndex, close}) {
    const history = useHistory();
    const {user, setUser} = useUserContext();
    const [name, setName] = useState('');
    const [chapter, setChapter] = useState(null);
    const [errors, setErrors] = useState([]);
    const [success, setSuccess] = useState("");
    const [onLoad, setOnLoad] = useState(false);

    const clearForm = () => {
        setName('');
        setSuccess('');
        setErrors([]);
    };

    function submitValue() {
        if(name) {
            setOnLoad(true);

            const form = {
                name: name
            };

            let idSubject =  null;
            if(classroomIndex !== null) {
                idSubject = user.classrooms[classroomIndex].subjects[subjectIndex].id;
            } else {
                idSubject = user.subjects[subjectIndex].id;
            }

            Api.postChapter(form, idSubject).then(response => {
                clearForm();
                setChapter(response.data.data);
                let newUser = {...user};

                if(classroomIndex !== null) {
                    newUser.classrooms[classroomIndex].subjects[subjectIndex].chapters.push(response.data.data);
                } else {
                    newUser.subjects[subjectIndex].chapters.push(response.data.data);
                }

                setUser(newUser);
                close();
            }).catch(error => {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors || {});
                }
            }).finally(() => {
                setOnLoad(false);
            });
        }
    };

    function newChapter() {
        setChapter(null);
    }

    function newSubChapter() {
        if(classroomIndex !== null) {
            history.push('/dashboard/classes/' + user.classrooms[classroomIndex].id +'/sujet/' + user.classrooms[classroomIndex].subjects[subjectIndex].id  + '/chapitre/' + chapter.id);
        } else {
            history.push('/dashboard/mes-cours/' + user.subjects[subjectIndex].id + '/chapitre/' + chapter.id);
        }
    }

    return (
        (chapter &&
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Nouveau chapitre</div>

                    <div className="black font-weight-bold text-center">
                        { chapter.name }
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="row">
                        <div className="col-6">
                            <button type="submit" className="btn-green-full btn-smaller" onClick={newSubChapter}>
                                Créer un sous chapitre
                            </button>
                        </div>
                        <div className="col-6">
                            <button type="submit" className="btn-green btn-smaller" onClick={newChapter}>
                                Créer un autre chapitre
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        ||
        <div>
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="grey text-center mt-4 font-size-12 mb-4">Créer un chapitre</div>

                    <div className="form-group input-no-border">
                        <input type="text" name="name" onChange={e => setName(e.target.value)} value={name}
                               className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')}
                               placeholder="Nom"/>
                        {(errors && errors.name) &&
                        <span className="invalid-feedback" role="alert"> {errors.name[0]} </span>}
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <div className="col-6 mx-auto">
                        <SubmitButton onLoad={onLoad} submit={() => submitValue()} disabled={!(!!name)}/>
                    </div>
                </div>
            </div>
        </div>
        )
    );
}

export default CreateChapterModal;
