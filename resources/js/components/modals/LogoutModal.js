import React from 'react';
import Api from "../services/Api";
import {useUserContext} from "../context/UserContext";

function LogoutModal() {
    const {user} = useUserContext();

    const submit = (e) => {
        e.preventDefault();

        Api.logout().then(response => {
            localStorage.removeItem('token');
            window.location.replace("/");
        }).catch(error => {
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });
    };

    return (
        <div className="position-relative">
            {!!user.student && (
                <div>
                    <div className="avatar-modal">
                        <div className={'avatar mx-auto'}>
                            <img src={user.avatar} title={'Avatar ' + user.name}/>
                        </div>
                    </div>
                    <div className="grey text-center mb-4 margin-top-60">Souhaitez-vous vous déconnecter ?</div>
                </div>
            ) || (
                <div className="grey text-center mb-4">Souhaitez-vous vous déconnecter ?</div>
            )}
            <div className="text-center">
                <a href="" className="btn-green-full btn-smaller" onClick={submit}>Déconnexion</a>
            </div>
        </div>
    );
}

export default LogoutModal;
