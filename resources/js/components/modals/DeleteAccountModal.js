import React from 'react';
import Api from "../services/Api";
import {useUserContext} from "../context/UserContext";

function DeleteAccountModal(props) {
    const {user} = useUserContext();

    const submit = (e) => {
        e.preventDefault();

        Api.deleteUser(props.user.id).then(response => {
            console.log(response);
            localStorage.removeItem('token');
            window.location.replace("/");
        }).catch(error => {
            if (error.response.status === 422) {
                console.log(error.response.data.errors || {});
            }
        });
    };

    return (
        <div className="position-relative">
            {!!user.student && (
                <div>
                    <div className="avatar-modal">
                        <div className={'avatar mx-auto'}>
                            <img src={user.avatar} title={'Avatar ' + user.name}/>
                        </div>
                    </div>
                    <div className="grey text-center font-size-12 margin-top-60">Souhaitez-vous supprimer définitivement votre compte ?</div>
                </div>
            ) || (
                <div className="grey text-center mt-4 font-size-12">Souhaitez-vous supprimer définitivement votre compte ?</div>
            )}

            <div className="blue text-center font-weight-semi-bold font-size-12">
                Attention toute votre progression et vos créations seront également supprimées,
                il sera impossible d’annuler cette opération.
            </div>
            <div className="text-center mt-2">
                <a href="" className="btn-green-full btn-smaller" onClick={submit} style={{'maxWidth': '200px'}}>Supprimer
                    le compte</a>
            </div>
        </div>
    );
}

export default DeleteAccountModal;
