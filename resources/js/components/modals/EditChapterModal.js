import React, {useState} from 'react';
import {useUserContext} from "../context/UserContext";
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import useForm from "../hooks/useForm";

function EditChapterModal({classroomIndex = null, subjectIndex, chapterIndex, closeModal}) {
    const {user, setUser} = useUserContext();
    const [classroomId, setClassroomId] = useState((classroomIndex !== null) ? user.classrooms[classroomIndex].id : null);

    const [form, setForm] = useForm(
        (classroomIndex !== null) ?
            user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex] : user.subjects[subjectIndex].chapters[chapterIndex]
    );

    const [chapter] = useForm(
        (classroomIndex !== null) ?
            user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex] : user.subjects[subjectIndex].chapters[chapterIndex]
    );

    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);
    const [errors, setErrors] = useState([]);

    const submit = () => {
        setOnLoad(true);

        Api.updateChapter(chapter.id, form).then(response => {
            let newUser = {...user};
            if(classroomId) {
                user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex] = response.data.data;
            } else {
                newUser.subjects[subjectIndex].chapters[chapterIndex] = response.data.data;
            }
            setUser(newUser);
            setSuccess(true);
            closeModal();
        }).catch(error => {
            if (error.response.status === 422) {
                setErrors(error.response.data.errors || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    return (
        <div>
            <div className="form-group input-no-border">
                <input type="text" name="name" onChange={setForm} value={form.name} className={'form-control' + ((errors && errors.name) ? ' is-invalid' : '')} placeholder="Nom"/>
                { (errors && errors.name) && <span className="invalid-feedback" role="alert"> {errors.name[0] } </span> }
            </div>

            <div className="d-flex justify-content-center">
                <SubmitButton onLoad={onLoad} submit={() => submit()} disabled={(form.name === chapter.name)} title={'Renommer'}/>
            </div>
        </div>
    );
}

export default EditChapterModal;
