import React, {useState, useEffect} from 'react';
import {useUserContext} from "../context/UserContext";
import Api from "../services/Api";
import SubmitButton from "../components/SubmitButton";
import { Redirect } from "react-router-dom";

function DeleteChapterModal({classroomIndex = null, subjectIndex, chapterIndex}) {
    const {user, setUser} = useUserContext();
    const [success, setSuccess] = useState(false);
    const [onLoad, setOnLoad] = useState(false);
    const [error, setError] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [newUser, setNewUser] = useState([]);

    useEffect( () => {
        if(success) {
            setUser(newUser);
            setRedirect(true);
        }
    }, [success]);

    const submit = () => {
        setOnLoad(true);

        let idChapter =  null;
        if(classroomIndex !== null) {
            idChapter = user.classrooms[classroomIndex].subjects[subjectIndex].chapters[chapterIndex].id;
        } else {
            idChapter = user.subjects[subjectIndex].chapters[chapterIndex].id;
        }

        Api.deleteChapter(idChapter).then(response => {
            let newData = {...user};

            if(classroomIndex !== null) {
                newData.classrooms[classroomIndex].subjects[subjectIndex].chapters.splice(chapterIndex, 1);
            } else {
                newData.subjects[subjectIndex].chapters.splice(chapterIndex, 1);
            }

            setNewUser(newData);
            setSuccess(true);
        }).catch(error => {
            if (error.response.status === 422) {
                setError(error.response.data.message || {});
            }
        }).finally(() => {
            setOnLoad(false);
        });
    };

    if(redirect) {
        if(classroomIndex !== null) {
            return <Redirect to={'/dashboard/classes/' + user.classrooms[classroomIndex].id + '/sujet/' + user.classrooms[classroomIndex].subjects[subjectIndex].id}/>
        } else {
            return <Redirect to={'/dashboard/mes-cours/' + user.subjects[subjectIndex].id}/>
        }
    }

    return (
        <div>
            <div className="grey text-center mb-4">Etes vous sur de supprimer ce chapitre ?</div>

            { (error) && <div className="text-danger text-center mb-3"> { error } </div> }

            <div className="d-flex justify-content-center">
                <SubmitButton onLoad={onLoad} submit={() => submit()} disabled={false} title={'Supprimer'}/>
            </div>
        </div>
    );
}

export default DeleteChapterModal;
