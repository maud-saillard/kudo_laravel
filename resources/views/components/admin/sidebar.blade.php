<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Menu</div>
                    <a class="nav-link @if(Route::currentRouteName() === 'admin.users.students') active @endif" href="{{ route('admin.users.students') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                        Etudiants
                    </a>

                    <a class="nav-link @if(Route::currentRouteName() === 'admin.users.tutors') active @endif" href="{{ route('admin.users.tutors') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-chalkboard-teacher"></i></div>
                        Tuteurs
                    </a>
                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Logged in as:</div>
                {{ Auth::user()->email }}
            </div>
        </nav>
    </div>

    {{ $slot }}
</div>
