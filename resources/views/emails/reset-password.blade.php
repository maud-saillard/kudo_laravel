@component('mail::message')
Hello,

Vous recevez ce message car nous avons reçu une demande pour réinitialiser votre mot de passe.

Le lien pour changer de mot de passe expirera dans {{ $count }}min
@component('mail::button', ['url' => $url])
Changer son mot de passe
@endcomponent

Si vous n'avez fait aucune demande, aucune action n'est requise.

Si vous rencontrez des problèmes pour cliquer sur le lien, copiez coller cet url dans votre navigateur :
<a href="{{ $url }}">{{ $url }}</a>

{{ config('app.name') }}
@endcomponent
