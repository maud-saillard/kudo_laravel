@component('mail::message')
Demande de contact

{{ $email }}<br>
{{ $name }}<br>
Université : {{ $university }}<br>
<br>
{{ $message }}

@component('mail::button', ['url' => route('home') ])
Kudo
@endcomponent

{{ config('app.name') }}
@endcomponent
