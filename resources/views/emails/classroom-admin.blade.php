@component('mail::message')
Bonjour {{ $classroom->admin_name }},<br>
Vous pouvez désormais modifier le cours <b>{{ $classroom->name }}</b> pendant la période suivant :<br>
Du {{ $classroom->date_start }} au {{ $classroom->date_end }}

@component('mail::button', ['url' => route('home') ])
Kudo
@endcomponent

{{ config('app.name') }}
@endcomponent
