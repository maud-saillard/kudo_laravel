@component('mail::message')
Hello,

Veuillez cliquer sur le lien ci-dessous pour vérifier votre email.

@component('mail::button', ['url' => $verificationUrl])
Kudo
@endcomponent

Si vous n'avez fait aucune demande, aucune action n'est requise.

Si vous rencontrez des problèmes pour cliquer sur le lien, copiez coller cet url dans votre navigateur :
<a href="{{ $verificationUrl }}">{{ $verificationUrl }}</a>

{{ config('app.name') }}
@endcomponent
