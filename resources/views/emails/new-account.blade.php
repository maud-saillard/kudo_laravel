@component('mail::message')
Bonjour {{ $user->name }},<br>
@if($user->tutor)
Vous pouvez désormais vous connecter en tant que <b>tuteur</b> sur <a href="{{ route('home') . '/login'  }}">Kudo</a> !
@else
Vous pouvez désormais vous connecter en tant qu'<b>étudiant</b> sur <a href="{{ route('home') . '/login' }}">Kudo</a> !
@endif

Votre mot de passe : <b>{{ $password }}</b>

@component('mail::button', ['url' => route('home') . '/login' ])
Se connecter
@endcomponent

{{ config('app.name') }}
@endcomponent
