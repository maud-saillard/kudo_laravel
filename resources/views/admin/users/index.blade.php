@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Utilisateurs</h1>

        <div class="m-4 d-flex flex-row-reverse">
            @if(Route::currentRouteName() === 'admin.users.students')
                <a type="button" class="btn btn-success" href="{{ route('admin.users.create', ['student']) }}">Ajouter</a>
            @else
                <a type="button" class="btn btn-success" href="{{ route('admin.users.create', ['tutor']) }}">Ajouter</a>
            @endif
        </div>

        <table id="data-table" class="table table-sm">
            <thead>
            <tr>
                <th scope="col">Avatar</th>
                <th scope="col">Nom</th>
                <th scope="col">Email</th>
                <th scope="col">Téléphone</th>
                @if(Route::currentRouteName() === 'admin.users.students')
                    <th scope="col">Date de naissance</th>
                @endif
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td style="width: 100px; height: 100px;">
                        @if($entity->avatar_url)
                            <img src="{{ $entity->avatar_url }}"
                                 alt="{{ $entity->name }}"
                                 style="object-fit: cover; width: 100%; height: 100%;">
                        @endif
                    </td>
                    <td>{{ $entity->name }}</td>
                    <td>{{ $entity->email }}</td>
                    <td>{{ $entity->phone }}</td>
                    @if(Route::currentRouteName() === 'admin.users.students')
                        <td>{{ $entity->birthday }}</td>
                    @endif
                    <td class="d-flex">
                        <a type="button" class="btn btn-primary mr-2" href="{{ route('admin.users.edit', ['user' => $entity->id]) }}">Modifier</a>
                        <form action="{{ route('admin.users.destroy', ['user' => $entity->id]) }}" method="POST" onSubmit="return confirm('Etes vous sur ?');">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')
    <script>
      $( document ).ready(function() {
        $('#data-table').DataTable();
      });
    </script>
@endsection
