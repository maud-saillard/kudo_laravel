@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Utilisateurs</h1>

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('admin.users.update', ['user' => $entity->id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="name">Type</label> <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="tutor" value="tutor"
                                   @if(old('type') && old('type') === 'tutor')
                                    checked
                                   @else
                                    @if($entity->tutor) checked @endif
                                   @endif>
                            <label class="form-check-label" for="tutor">Tuteur</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="student" value="student"
                                   @if(old('type') && old('type') === 'student')
                                   checked
                                   @else
                                    @if($entity->student) checked @endif
                                   @endif>
                            <label class="form-check-label" for="student">Etudiant</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
                               placeholder="name" value="{{ $entity->name ?? old('name') }}" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                               id="email" value="{{ $entity->email ?? old('email') }}" placeholder="email" required>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="phone">Téléphone</label>
                        <input type="tel" name="phone" class="form-control @error('phone') is-invalid @enderror"
                               id="phone" value="{{ $entity->phone ?? old('phone') }}" placeholder="Téléphone">

                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="birthday">Année de naissance</label>
                        <input type="date" name="birthday" class="form-control @error('birthday') is-invalid @enderror"
                               id="birthday" value="{{ $entity->birthday ?? old('birthday') }}"
                               placeholder="Année de naissance">

                        @error('birthday')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="avatar">Avatar</label>
                        <input type="file" class="form-control" id="avatar" name="avatar">
                    </div>

                    <button type="submit" class="btn btn-primary">Modifier</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/summernote-bs4.min.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('#summernote').summernote({
          height: 400,
          toolbar: [
            ["style", ["style"]],
            ["font", ["bold", "underline", "clear"]],
            ["fontname", ["fontname"]],
            ["color", ["color"]],
            ["para", ["ul", "ol", "paragraph"]],
            ["table", ["table"]],
            ["insert", ["link", "picture"]],
            ["view", ["fullscreen", "codeview", "help"]]
          ],
          defaultFontName: 'Yantramanav',
          fontNames: [
            'Yantramanav', 'Crimson Text', 'Arial', 'Arial Black', 'Times New Roman', 'Verdana',
          ],
          fontNamesIgnoreCheck: ['Yantramanav', 'Crimson Text'],
        });
      });
    </script>
@endsection

