<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $fillable = ['name', 'date_start', 'date_end'];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:50'],
    ];

    public const VALIDATION_ADMIN_RULES = [
        'admin_email' => ['required', 'string', 'email', 'max:255'],
        'date_start' => ['required', 'date'],
        'date_end' => ['required', 'date'],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function subjects()
    {
        return $this->hasMany('App\Subject')->orderBy('created_at', 'DESC');
    }

    public function students()
    {
        return $this->belongsToMany('App\User', 'classroom_user');
    }
}
