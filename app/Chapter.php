<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = ['name'];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:50'],
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($chapter)
        {
            $chapter->subChapters()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function subChapters()
    {
        return $this->hasMany('App\SubChapter')->orderBy('order', 'ASC');
    }
}
