<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name'];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:50'],
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($subject)
        {
            $subject->chapters()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter')->orderBy('order', 'ASC');
    }

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }
}
