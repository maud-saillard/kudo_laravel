<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ClassroomResource extends JsonResource
{
    public $preserveKeys = true;

    public function toArray($request)
    {
        if($request->user()->id === $this->user_id) {
            $students = StudentResource::collection($this->students);

            $adminId = $this->admin ? $this->admin->id : null;
            $adminEmail = $this->admin ? $this->admin->email : null;
            $adminName = $this->admin ? $this->admin->name : null;
        } else {
            $students = StudentResource::collection($this->students);

            $adminId = null;
            $adminEmail = null;
            $adminName = null;

            $now = Carbon::now()->toDateString();
            if($this->date_start && $this->date_end) {
                $adminId = $now;
                if($this->date_start <= $now && $this->date_end >= $now) {
                    $adminId = $this->admin ? $this->admin->id : null;
                    $adminEmail = $this->admin ? $this->admin->email : null;
                    $adminName = $this->admin ? $this->admin->name : null;
                }
            }
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_id' => $this->user_id,
            'admin_id' => $adminId,
            'admin_email' => $adminEmail,
            'admin_name' => $adminName,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'subjects' => SubjectResource::collection($this->subjects),
            'students' => $students,
        ];
    }
}
