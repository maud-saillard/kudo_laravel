<?php

namespace App\Http\Resources;

use App\Classroom;
use App\ClassroomUser;
use App\Subject;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public $preserveKeys = true;

    public function toArray($request)
    {
        $nb = 0;
        if($this->student) {

            $data = Subject::with('chapters')
                ->with('chapters.subChapters')
                ->with('chapters.subChapters.cards')
                ->where('user_id', $this->id)
                ->where('classroom_id', null)
                ->get();

            $idClassrooms = ClassroomUser::where('user_id', $this->id)->pluck('classroom_id')->toArray();

            $classrooms = Classroom::with('subjects')
                ->with('subjects.chapters')
                ->with('subjects.chapters.subChapters')
                ->with('subjects.chapters.subChapters.cards')
                ->with('students')
                ->whereIn('id', $idClassrooms)->get();

            $subjects = SubjectResource::collection($data);
            $classrooms = ClassroomResource::collection($classrooms);
        } else {
            $subjects = [];

            $classrooms = Classroom::with('subjects')
                ->with('subjects.chapters')
                ->with('subjects.chapters.subChapters')
                ->with('subjects.chapters.subChapters.cards')
                ->where('user_id', $this->id)
                ->get();

            $classrooms = ClassroomResource::collection($classrooms);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'birthday' => $this->birthday,
            'avatar' => $this->avatar_url,
            'student' => $this->student,
            'tutor' => $this->tutor,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'subjects' => $subjects,
            'classrooms' => $classrooms,
            'nb_students' => $nb
        ];
    }
}
