<?php

namespace App\Http\Requests;

use App\SubChapter;
use Illuminate\Foundation\Http\FormRequest;

class StoreSubChapterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = SubChapter::VALIDATION_RULES;

        return $rules;
    }
}
