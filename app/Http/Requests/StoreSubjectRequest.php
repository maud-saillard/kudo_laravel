<?php

namespace App\Http\Requests;

use App\Subject;
use Illuminate\Foundation\Http\FormRequest;

class StoreSubjectRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = Subject::VALIDATION_RULES;

        return $rules;
    }
}
