<?php

namespace App\Http\Requests;

use App\Subject;
use Illuminate\Foundation\Http\FormRequest;

class StoreClassroomRequest extends FormRequest
{
    public function authorize()
    {
        if ($this->user()->tutor) {
            return true;
        } else {
            return false;
        }
    }

    public function rules()
    {
        $rules = Subject::VALIDATION_RULES;

        return $rules;
    }
}
