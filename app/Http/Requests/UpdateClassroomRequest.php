<?php

namespace App\Http\Requests;

use App\Classroom;
use App\Subject;
use Illuminate\Foundation\Http\FormRequest;

class UpdateClassroomRequest extends FormRequest
{
    public function authorize()
    {
        if ($this->user()->tutor) {
            return true;
        } else {
            return false;
        }
    }

    public function rules()
    {
        if($this->route()->getName() === 'classrooms.update.admin') {
            $rules = Classroom::VALIDATION_ADMIN_RULES;
        } else {
            $rules = Classroom::VALIDATION_RULES;
        }

        return $rules;
    }
}
