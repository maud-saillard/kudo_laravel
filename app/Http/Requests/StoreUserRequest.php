<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        if ($this->user()->id == $this->route('id')) {
            return true;
        } else {
            return false;
        }
    }

    public function rules()
    {
        if($this->getMethod() === 'POST')
        {
            $rules = User::VALIDATION_RULES + [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:4'],
            ];
        } else {
            $rules = User::VALIDATION_RULES + [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . request()->route('id')]
                ];

            if(request()->password) {
                $rules = $rules + ['password' => ['sometimes', 'string', 'min:4']];
            }
        }

        return $rules;
    }
}
