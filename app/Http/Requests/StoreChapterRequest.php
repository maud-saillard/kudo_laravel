<?php

namespace App\Http\Requests;

use App\Chapter;
use Illuminate\Foundation\Http\FormRequest;

class StoreChapterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = Chapter::VALIDATION_RULES;

        return $rules;
    }
}
