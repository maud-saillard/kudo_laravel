<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Classroom;
use App\Http\Requests\StoreSubjectRequest;
use App\Http\Resources\SubjectResource;
use App\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class SubjectController extends Controller
{
    public function index(Request $request)
    {
        return SubjectResource::collection($request->user()->subjects);
    }

    public function show(Subject $subject)
    {
        return new SubjectResource($subject);
    }

    public function store(StoreSubjectRequest $request)
    {
        $subjects = Subject::where('classroom_id', null)->where('user_id', $request->user()->id)->orderBy('order')->count();

        $entity = new Subject();
        $entity->fill($request->all());
        $entity->order = $subjects;
        $entity->user()->associate($request->user())->save();

        if($request->classroom_id) {
            $classroom = Classroom::where('id', $request->classroom_id)->first();
            if($classroom) {
                $entity->order = $classroom->subjects->count();
                $entity->classroom()->associate($classroom)->save();
            }
        }

        return new SubjectResource($entity);
    }

    public function update(StoreSubjectRequest $request, Subject $subject)
    {
        if($this->checkRights($subject, $request->user())) {
            $subject->update($request->all());
            return new SubjectResource($subject);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cette matière'], 422);
    }

    public function destroy(Request $request, Subject $subject)
    {
        if($this->checkRights($subject, $request->user())) {

            if(isset($subject->classroom)) {
                $classroomId = $subject->classroom->id;
            }

            $subject->delete();

            if(isset($classroomId)) {
                $subjects = Subject::where('classroom_id', $classroomId)->orderBy('order')->get();
            } else {
                $subjects = Subject::where('classroom_id', null)->where('user_id', $request->user()->id)->orderBy('order')->get();
            }

            foreach($subjects as $key => $subject) {
                $subject->order = $key;
                $subject->save();
            }

            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer cette matière'], 422);
    }

    private function checkRights($subject, $user)
    {
        $now = Carbon::now()->toDateString();

        if($subject->classroom) {
            if($subject->classroom->user_id === $user->id) {
                return true;
            }

            if($subject->classroom->admin_id === $user->id) {
                if($subject->classroom->date_start <= $now && $subject->classroom->date_end >= $now) {
                    return true;
                }
            }

            return false;
        }

        if($subject->user_id === $user->id) {
            return true;
        }

        return false;
    }
}
