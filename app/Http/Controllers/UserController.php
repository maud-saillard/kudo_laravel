<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Classroom;
use App\ClassroomUser;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\SubChapter;
use App\Subject;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    public function index()
    {
        return new UserCollection(User::all());
    }

    public function show(Request $request)
    {
        return new UserResource($request->user());
    }

    public function update(StoreUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());

        return new UserResource($user);
    }

    public function updateAvatar(Request $request, $id)
    {
        $user = User::find($id);

        if($user->id !== $request->user()->id) {
            return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cet utilisateur'], 422);
        }

        $this->validate($request, [
            'avatar' => 'required|image',
        ]);

        Storage::delete($user->avatar);

        $path = $request->file('avatar')->store('avatars');
        $user->avatar = $path;
        $user->save();

        return new JsonResponse(['data' => $user->avatar_url], 201);
    }

    public function destroy(Request $request, $id)
    {
        $user = User::find($id);

        if($user->id === $request->user()->id) {
            $user->tokens()->where('id', $request->user()->currentAccessToken()->id)->delete();
            $user->delete();
            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer cet utilisateur'], 422);
    }

    public function stats(Request $request, $classroomId = null)
    {
        $user = $request->user();

        if($classroomId) {
            $classroomsId = Classroom::where('id', $classroomId)->where('user_id', $user->id)->pluck('id')->toArray();
        } else {
            $classroomsId = Classroom::where('user_id', $user->id)->pluck('id')->toArray();
        }

        $subjects = Subject::whereIn('classroom_id', $classroomsId)->pluck('id')->toArray();
        $chapters = Chapter::whereIn('subject_id', $subjects)->pluck('id')->toArray();
        $subChapters = SubChapter::whereIn('chapter_id', $chapters)->pluck('id')->toArray();
        $admins = Classroom::where('user_id', $user->id)->whereNotNull('admin_id')->count();
        $classroomUsers = ClassroomUser::whereIn('classroom_id', $classroomsId)->count();

        $array = [
            'nb_classrooms' => count($classroomsId),
            'nb_subjects' => count($subjects),
            'nb_chapters' => count($chapters),
            'nb_sub_chapters' => count($subChapters),
            'nb_admins' => $admins,
            'nb_students' => $classroomUsers,
        ];

        return new JsonResponse(['data' => $array], 201);
    }

    public function lastPosts(Request $request, $classroomId = null)
    {
        $user = $request->user();

        if($classroomId) {
            $classroomsId = Classroom::where('id', $classroomId)->where('user_id', $user->id)->pluck('id')->toArray();
        } else {
            $classroomsId = Classroom::where('user_id', $user->id)->pluck('id')->toArray();
        }

        $subjectsId = Subject::whereIn('classroom_id', $classroomsId)->pluck('id')->toArray();
        $chaptersId = Chapter::whereIn('subject_id', $subjectsId)->pluck('id')->toArray();
        $subChaptersId = SubChapter::whereIn('chapter_id', $chaptersId)->pluck('id')->toArray();

        $subjects = Subject::with('classroom')
            ->whereIn('classroom_id', $classroomsId)
            ->orderBy('created_at', 'DESC')
            ->limit(5)->get();

        $chapters = Chapter::with('subject')->with('subject.classroom')
            ->whereIn('subject_id', $subjectsId)
            ->orderBy('created_at', 'DESC')
            ->limit(5)->get();

        $subChapters = SubChapter::with('chapter')
            ->with('chapter.subject')
            ->with('chapter.subject.classroom')
            ->whereIn('chapter_id', $chaptersId)
            ->orderBy('created_at', 'DESC')
            ->limit(5)->get();

        $data = [];

        foreach ($subjects as $entity) {
            $data[Carbon::parse($entity->created_at)->timestamp] = [
                'id' => $entity->id,
                'name' => $entity->name,
                'type' => 'subject',
                'created_at' => $entity->created_at,
                'classroom' => $entity->classroom->name,
                'classroom_id' => $entity->classroom->id,
                'subject' => $entity->name,
                'subject_id' => $entity->id,
            ];
        }

        foreach ($chapters as $entity) {
            $data[Carbon::parse($entity->created_at)->timestamp] = [
                'id' => $entity->id,
                'name' => $entity->name,
                'type' => 'chapter',
                'created_at' => $entity->created_at,
                'classroom' => $entity->subject->classroom->name,
                'classroom_id' => $entity->subject->classroom->id,
                'subject' => $entity->subject->name,
                'subject_id' => $entity->subject->id,
            ];
        }

        foreach ($subChapters as $entity) {
            $data[Carbon::parse($entity->created_at)->timestamp] = [
                'id' => $entity->id,
                'name' => $entity->name,
                'type' => 'sub-chapter',
                'created_at' => $entity->created_at,
                'classroom' => $entity->chapter->subject->classroom->name,
                'classroom_id' => $entity->chapter->subject->classroom->id,
                'subject' => $entity->chapter->subject->name,
                'subject_id' => $entity->chapter->subject->id,
                'chapter_id' => $entity->chapter->id,
            ];
        }

        ksort($data);
        $data = array_slice($data, -5);

        return new JsonResponse(['data' => $data], 201);
    }
}
