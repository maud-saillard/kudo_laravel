<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        return view('pages.home');
    }

    public function contact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'university' => 'required|string',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_CONTACT'))->send(new Contact($request));

        return response()->json(null, 200);
    }

    public function sendVerificationEmail(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();
        return new Response('', 204);
    }

    public function sendResetPassword(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();
        return new Response('', 204);
    }
}
