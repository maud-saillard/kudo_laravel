<?php

namespace App\Http\Controllers;

use App\Card;
use App\Chapter;
use App\Helpers\Helper;
use App\Http\Requests\StoreChapterRequest;
use App\Http\Requests\StoreSubjectRequest;
use App\Http\Resources\ChapterResource;
use App\Http\Resources\SubjectResource;
use App\SubChapter;
use App\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ChapterController extends Controller
{
    public function index(Request $request, Subject $subject)
    {
        return ChapterResource::collection($subject->chapters);
    }

    public function show(Chapter $chapter)
    {
        return new ChapterResource($chapter);
    }

    public function store(StoreChapterRequest $request, Subject $subject)
    {
        $entity = new Chapter();

        $entity->fill($request->all());
        $entity->order = $subject->chapters->count();
        $entity->subject()->associate($subject);
        $entity->user()->associate($request->user())->save();

        return new ChapterResource($entity);
    }

    public function update(StoreChapterRequest $request, Chapter $chapter)
    {
        if($this->checkRights($chapter, $request->user())) {
            $chapter->update($request->all());
            return new ChapterResource($chapter);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier ce chapitre'], 422);
    }

    public function destroy(Request $request, Chapter $chapter)
    {
        if($this->checkRights($chapter, $request->user())) {
            $subjectId = $chapter->subject->id;
            $chapter->delete();

            $chapters = Chapter::where('subject_id', $subjectId)->orderBy('order')->get();

            foreach($chapters as $key => $chapter) {
                $chapter->order = $key;
                $chapter->save();
            }

            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer ce chapitre'], 422);
    }

    public function updateOrder(Request $request, Chapter $chapter)
    {
        if($this->checkRights($chapter, $request->user())) {

            if(!isset($request->order) OR !is_int($request->order)) {
                return new JsonResponse(['message' => 'L\'ordre est incorrect'], 422);
            }

            $entities = Chapter::where('subject_id', $chapter->subject->id)->orderBy('order')->get();

            Helper::reorder($entities, $chapter->order, $request->order);

            return new ChapterResource($chapter);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier ce chapitre'], 422);
    }

    private function checkRights($chapter, $user)
    {
        $now = Carbon::now()->toDateString();

        if($chapter->subject->classroom) {
            if($chapter->subject->classroom->user_id === $user->id) {
                return true;
            }

            if($chapter->subject->classroom->admin_id === $user->id) {
                if($chapter->subject->classroom->date_start <= $now && $chapter->subject->classroom->date_end >= $now) {
                    return true;
                }
            }

            return false;
        }

        if($chapter->user_id === $user->id) {
            return true;
        }

        return false;
    }
}
