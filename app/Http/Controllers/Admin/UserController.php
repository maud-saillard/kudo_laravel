<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\NewAccount;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    protected $entity;
    protected $rules;
    protected $folder;
    protected $name;

    public function __construct()
    {
        $this->entity = 'App\User';
        $this->folder = 'users';
        $this->name = 'Utilisateur';
        $this->rules = User::VALIDATION_RULES;
    }

    public function students()
    {
        $entities = $this->entity::where('student', true)->orderBy('created_at', 'DESC')->get();

        return view('admin.' . $this->folder . '.index',
            ['entities' => $entities]
        );
    }

    public function tutors()
    {
        $entities = $this->entity::where('tutor', true)->orderBy('created_at', 'DESC')->get();

        return view('admin.' . $this->folder . '.index',
            ['entities' => $entities]
        );
    }

    public function show($entity)
    {
        return redirect()->route('admin.' . $this->folder . '.index');
    }

    public function create(Request $request)
    {
        return view('admin.' . $this->folder . '.create', ['request' => $request]);
    }

    public function store(Request $request)
    {
        $inputs = $request->all();
        $rules = array_merge($this->rules, User::VALIDATION_EMAIL);
        Validator::make($inputs, $rules)->validate();

        $entity = new $this->entity();
        $inputs['password'] = Str::random(8);

        if($inputs['type'] === 'tutor') {
            $entity->tutor = true;
        } else {
            $entity->student = true;
        }

        $entity->fill($inputs)->save();

        $entity->email_verified_at = Carbon::now();
        $entity->save();

        if($request->file('avatar')) {
            Storage::delete($entity->avatar);

            $path = $request->file('avatar')->store('avatars');
            $entity->avatar = $path;
            $entity->save();
        }

        Mail::to($entity->email)->send(new NewAccount($entity, $inputs['password']));

        if($entity->tutor) {
            return redirect()->route('admin.' . $this->folder . '.tutors')->with('success', $this->name . ' ajouté !');
        }

        return redirect()->route('admin.' . $this->folder . '.students')->with('success', $this->name . ' ajouté !');
    }

    public function edit($entity)
    {
        $entity = $this->entity::where('id', $entity)->first();

        if(!$entity) {
            Session::flash('error', 'L\''. $this->name .' n\'existe pas');
            return redirect()->back();
        }

        return view('admin.' . $this->folder . '.edit',
            ['entity' => $entity]
        );
    }

    public function update(Request $request, $entity)
    {
        $entity = $this->entity::where('id', $entity)->first();

        if (!$entity) {
            Session::flash('error', 'L\''. $this->name .' n\'existe pas');
            return redirect()->back();
        }

        $inputs = $request->all();
        Validator::make($inputs, $this->rules)->validate();

        if($inputs['type'] === 'tutor') {
            $entity->student = false;
            $entity->tutor = true;
        } else {
            $entity->student = true;
            $entity->tutor = false;
        }

        $entity->fill($inputs)->save();

        if($request->file('avatar')) {
            Storage::delete($entity->avatar);

            $path = $request->file('avatar')->store('avatars');
            $entity->avatar = $path;
            $entity->save();
        }

        if($entity->tutor) {
            return redirect()->route('admin.' . $this->folder . '.tutors')->with('success', $this->name . ' supprimé !');
        }

        return redirect()->route('admin.' . $this->folder . '.students')->with('success', $this->name . ' modifié !');
    }

    public function destroy($entity)
    {
        $entity = $this->entity::where('id', $entity)->first();

        if(!$entity) {
            Session::flash('error', 'L\''. $this->name .' n\'existe pas');
            return redirect()->back();
        }

        $tutor = $entity->tutor;

        $entity->delete();

        if($tutor) {
            return redirect()->route('admin.' . $this->folder . '.tutors')->with('success', $this->name . ' supprimé !');
        }

        return redirect()->route('admin.' . $this->folder . '.students')->with('success', $this->name . ' supprimé !');
    }
}
