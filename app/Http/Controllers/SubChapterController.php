<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Helpers\Helper;
use App\Http\Requests\StoreChapterRequest;
use App\Http\Requests\StoreSubChapterRequest;
use App\Http\Requests\StoreSubjectRequest;
use App\Http\Resources\ChapterResource;
use App\Http\Resources\SubChapterResource;
use App\Http\Resources\SubjectResource;
use App\SubChapter;
use App\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class SubChapterController extends Controller
{
    public function index(Request $request, Chapter $chapter)
    {
        return ChapterResource::collection($chapter->subChapters);
    }

    public function show(SubChapter $subChapter)
    {
        return new SubChapterResource($subChapter);
    }

    public function store(StoreSubChapterRequest $request, Chapter $chapter)
    {
        $entity = new SubChapter();

        $entity->fill($request->all());
        $entity->order = $chapter->subChapters->count();
        $entity->chapter()->associate($chapter);
        $entity->user()->associate($request->user())->save();

        return new SubChapterResource($entity);
    }

    public function update(StoreSubChapterRequest $request, SubChapter $subChapter)
    {
        if($this->checkRights($subChapter, $request->user())) {
            $subChapter->update($request->all());
            return new SubChapterResource($subChapter);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier ce sous-chapitre'], 422);
    }

    public function destroy(Request $request, SubChapter $subChapter)
    {
        if($this->checkRights($subChapter, $request->user())) {
            $chapterId = $subChapter->chapter->id;
            $subChapter->delete();

            $subChapters = SubChapter::where('chapter_id', $chapterId)->orderBy('order')->get();

            foreach($subChapters as $key => $subChapter) {
                $subChapter->order = $key;
                $subChapter->save();
            }

            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer ce sous-chapitre'], 422);
    }

    public function updateOrder(Request $request, SubChapter $subChapter)
    {
        if($this->checkRights($subChapter, $request->user())) {

            if(!isset($request->order) OR !is_int($request->order)) {
                return new JsonResponse(['message' => 'L\'ordre est incorrect'], 422);
            }

            $entities = SubChapter::where('chapter_id', $subChapter->chapter->id)->orderBy('order')->get();

            Helper::reorder($entities, $subChapter->order, $request->order);

            return new SubChapterResource($subChapter);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier ce sous-chapitre'], 422);
    }

    private function checkRights($subChapter, $user)
    {
        $now = Carbon::now()->toDateString();

        if($subChapter->chapter->subject->classroom) {
            if($subChapter->chapter->subject->classroom->user_id === $user->id) {
                return true;
            }

            if($subChapter->chapter->subject->classroom->admin_id === $user->id) {
                if($subChapter->chapter->subject->classroom->date_start <= $now && $subChapter->chapter->subject->classroom->date_end >= $now) {
                    return true;
                }
            }

            return false;
        }

        if($subChapter->user_id === $user->id) {
            return true;
        }

        return false;
    }
}
