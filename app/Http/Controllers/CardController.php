<?php

namespace App\Http\Controllers;

use App\Card;
use App\Helpers\Helper;
use App\Http\Requests\StoreCardRequest;
use App\Http\Resources\CardResource;
use App\Http\Resources\SubChapterResource;
use App\SubChapter;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\JsonResponse;

class CardController extends Controller
{
    public function index(Request $request, SubChapter $subChapter)
    {
        return SubChapterResource::collection($subChapter->cards);
    }

    public function show(Card $card)
    {
        return new CardResource($card);
    }

    public function store(StoreCardRequest $request, SubChapter $subChapter)
    {
        $entity = new Card();

        $entity->fill($request->all());
        $entity->order = $subChapter->cards->count();
        $entity->subChapter()->associate($subChapter);
        $entity->user()->associate($request->user())->save();

        if($request->file('file')) {
            $this->validate($request, [
                'file' => 'required|image',
            ]);

            Storage::delete($entity->file);

            $path = $request->file('file')->store('files');
            $entity->file = $path;
            $entity->save();
        }

        return new CardResource($entity);
    }

    public function update(StoreCardRequest $request, Card $card)
    {
        if($this->checkRights($card, $request->user())) {
            $card->update($request->all());

            if($request->file('file')) {
                $this->validate($request, [
                    'file' => 'required|image',
                ]);

                Storage::delete($card->file);

                $path = $request->file('file')->store('files');
                $card->file = $path;
                $card->save();
            }

            return new CardResource($card);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cette carte'], 422);
    }

    public function destroy(Request $request, Card $card)
    {
        if($this->checkRights($card, $request->user())) {

            $subChapterId = $card->subChapter->id;
            $card->delete();

            $cards = Card::where('sub_chapter_id', $subChapterId)->orderBy('order')->get();

            foreach($cards as $key => $card) {
                $card->order = $key;
                $card->save();
            }

            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer cette carte'], 422);
    }

    public function updateOrder(Request $request, Card $card)
    {
        if($this->checkRights($card, $request->user())) {

            if(!isset($request->order) OR !is_int($request->order)) {
                return new JsonResponse(['message' => 'L\'ordre est incorrect'], 422);
            }

            $entities = Card::where('sub_chapter_id', $card->subChapter->id)->orderBy('order')->get();

            Helper::reorder($entities, $card->order, $request->order);

            return new CardResource($card);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cette carte'], 422);
    }

    private function checkRights($card, $user)
    {
        $now = Carbon::now()->toDateString();

        if($card->subChapter->chapter->subject->classroom) {
            if($card->subChapter->chapter->subject->classroom->user_id === $user->id) {
                return true;
            }

            if($card->subChapter->chapter->subject->classroom->admin_id === $user->id) {
                if($card->subChapter->chapter->subject->classroom->date_start <= $now && $card->subChapter->chapter->subject->classroom->date_end >= $now) {
                    return true;
                }
            }

            return false;
        }

        if($card->user_id === $user->id) {
            return true;
        }

        return false;
    }
}
