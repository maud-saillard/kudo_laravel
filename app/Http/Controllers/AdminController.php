<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user) {
            return redirect()->route('admin.login')->with('error', 'Cet utilisateur n\'existe pas');
        }

        if(!$user->isAdmin()) {
            return redirect()->route('admin.login')->with('error', 'Cet utilisateur n\'est pas admin');
        }

        if (!$user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Les identifiants fournis sont incorrects'],
            ]);
        }

        Auth::loginUsingId($user->id);

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->intended(route('admin.index'));
    }

    public function logout(Request $request)
    {
        Auth::logout();

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->route('home');
    }

    public function index()
    {
        return redirect()->route('admin.users.students');
    }
}
