<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Http\Requests\StoreClassroomRequest;
use App\Http\Requests\UpdateClassroomRequest;
use App\Http\Resources\ClassroomResource;
use App\Mail\ClassroomAdmin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClassroomController extends Controller
{
    public function index(Request $request)
    {
        return ClassroomResource::collection($request->user()->classrooms);
    }

    public function show(Classroom $classroom)
    {
        return new ClassroomResource($classroom);
    }

    public function store(StoreClassroomRequest $request)
    {
        $entity = new Classroom();
        $entity->fill($request->validated());
        $entity->user()->associate($request->user())->save();

        return new ClassroomResource($entity);
    }

    public function update(UpdateClassroomRequest $request, Classroom $classroom)
    {
        if($classroom->user->id === $request->user()->id) {
            $classroom->update($request->validated());
            return new ClassroomResource($classroom);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cette classe'], 422);
    }

    public function updateAdmin(UpdateClassroomRequest $request, Classroom $classroom)
    {
        if($classroom->user->id === $request->user()->id) {
            $admin = User::where('email', $request->admin_email)->first();

            if(!$admin) {
                return new JsonResponse(['message' => 'Cet utilisateur n\'existe pas'], 422);
            }

            $classroom->update($request->validated());
            $classroom->admin()->associate($admin)->save();
            Mail::to($admin->email)->send(new ClassroomAdmin($classroom));
            return new ClassroomResource($classroom);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas modifier cette classe'], 422);
    }

    public function destroy(Request $request, Classroom $classroom)
    {
        if($classroom->user->id === $request->user()->id) {
            $classroom->delete();
            return new Response('', 201);
        }

        return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer cette classe'], 422);
    }

    public function addStudents(Request $request, Classroom $classroom)
    {
        if($classroom->user->id !== $request->user()->id) {
            return new JsonResponse(['message' => 'Vous ne pouvez pas ajouter d\'étudiant'], 422);
        }

        if(!$request->students) {
            return new JsonResponse(['message' => 'Liste vide'], 422);
        }

        $students = $request->students;

        $list = preg_split ('/(;|,|\n|\*)/', $students);

        foreach ($list as $email) {
            $user = User::where('email', $email)->first();
            if($user) {
                $classroom->students()->sync($user, false);
            }
        }

        return new ClassroomResource($classroom);
    }

    public function removeStudent(Request $request, Classroom $classroom, User $user)
    {
        if($classroom->user->id !== $request->user()->id) {
            return new JsonResponse(['message' => 'Vous ne pouvez pas supprimer d\'étudiant'], 422);
        }

        if(!$user) {
            return new JsonResponse(['message' => 'Cet étudiant n\'existe pas'], 422);
        }

        $classroom->students()->detach($user);

        return new ClassroomResource($classroom);
    }
}
