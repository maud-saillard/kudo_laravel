<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubChapter extends Model
{
    protected $fillable = ['name'];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:50'],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }

    public function cards()
    {
        return $this->hasMany('App\Card')->orderBy('order', 'ASC');
    }
}
