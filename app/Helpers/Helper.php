<?php

namespace App\Helpers;

class Helper
{
    public static function reorder($entities, $oldPosition, $newPosition)
    {
        $p1 = $entities->splice($oldPosition,1);
        $p2 = $entities->splice(0, $newPosition);

        $entities = $p2->merge($p1->merge($entities));

        foreach($entities as $key => $entity) {
            $entity->order = $key;
            $entity->save();
        }

        return true;
    }
}
