<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['name', 'content'];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:50'],
        'content' => ['string'],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subChapter()
    {
        return $this->belongsTo('App\SubChapter');
    }

    public function getFileUrlAttribute()
    {
        return $this->file ? asset('storage/' . $this->file) : null;
    }
}
