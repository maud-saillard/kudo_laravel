<?php

namespace App;

use App\Http\Notifications\ResetPassword;
use App\Http\Notifications\VerifyEmail;
use App\Mail\Contact;
use App\Mail\Register;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'birthday', 'student', 'tutor'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public const VALIDATION_EMAIL = [
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    ];

    public const VALIDATION_RULES = [
        'name' => ['required', 'string', 'max:100'],
        'phone' => ['nullable', 'numeric', 'digits_between:10,11'],
        'birthday' => ['nullable', 'date_format:Y-m-d', 'after_or_equal:1920-01-01']
    ];

    public function getAvatarUrlAttribute()
    {
        return $this->avatar ? asset('storage/' . $this->avatar) : asset('images/default-avatar.png');
    }

    public function setPasswordAttribute($value)
    {
        if($value && $value !== '') {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function classrooms()
    {
        return $this->hasMany('App\Classroom')->orderBy('created_at', 'DESC');
    }

    public function subjects()
    {
        return $this->hasMany('App\Subject')->orderBy('created_at', 'DESC');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter')->orderBy('order', 'ASC');
    }

    public function studentClassrooms()
    {
        return $this->belongsToMany('App\Classroom', 'classroom_user');
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function isAdmin() {
        return $this->admin;
    }
}
