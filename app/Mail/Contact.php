<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $university;
    public $message;

    public function __construct($data)
    {
        $this->name = $data->name;
        $this->email = $data->email;
        $this->university = $data->university;
        $this->message = $data->message;
    }

    public function build()
    {
        return $this->markdown('emails.contact')->subject('Demande de contact');
    }
}
