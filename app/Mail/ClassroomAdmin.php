<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClassroomAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $classroom;

    public function __construct($classroom)
    {
        $this->classroom = $classroom;
    }

    public function build()
    {
        return $this->markdown('emails.classroom-admin')->subject('Invitation à modifier le cours ' . $this->classroom->name);
    }
}
