<?php

use Illuminate\Support\Facades\Route;

Route::get('/admin/login', 'AdminController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'AdminController@login')->name('admin.login');
Route::post('/admin/logout', 'AdminController@logout')->name('admin.logout');

Route::middleware(['auth', 'is_admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('index');

    Route::get('/users', 'Admin\UserController@students')->name('users.students');
    Route::get('/users/tutor', 'Admin\UserController@tutors')->name('users.tutors');
    Route::get('/users/create/{type?}', 'Admin\UserController@create')->name('users.create');
    Route::post('/users', 'Admin\UserController@store')->name('users.store');
    Route::get('/users/edit/{user}', 'Admin\UserController@edit')->name('users.edit');
    Route::put('/users/{user}', 'Admin\UserController@update')->name('users.update');
    Route::delete('/users/{user}', 'Admin\UserController@destroy')->name('users.destroy');
});

Route::get('/dashboard/{params?}', 'DashboardController@index')->where('params', '(.*)');
Route::get('/reset/{token}/{email}', 'HomeController@index')->name('reset');
Route::get('/{path?}', 'HomeController@index')->name('home');
