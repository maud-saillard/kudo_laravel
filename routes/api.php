<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Auth::routes();

Auth::routes(['verify' => true]);
Route::post('/contact', 'HomeController@contact')->name('contact');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/send-verification-email', 'HomeController@sendVerificationEmail')->name('send-verification-email');
});

Route::middleware(['verified'])->group(function () {
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/user', 'UserController@show')->name('users.show');
        Route::get('/user/stats/{classroomId?}', 'UserController@stats')->name('users.stats');
        Route::get('/user/last-posts/{classroomId?}', 'UserController@lastPosts')->name('users.last-posts');

        Route::prefix('users')->name('users.')->group(function () {
            Route::post('/avatar/{id}', 'UserController@updateAvatar')->name('update.avatar');
            Route::put('/{id}', 'UserController@update')->name('update');
            Route::delete('/{id}', 'UserController@destroy')->name('destroy');
            Route::get('/', 'UserController@index')->name('index');
        });

        Route::prefix('classrooms')->name('classrooms.')->group(function () {
            Route::put('/students/{classroom}', 'ClassroomController@addStudents')->name('students.add');
            Route::delete('/students/{classroom}/{user}', 'ClassroomController@removeStudent')->name('students.remove');
            Route::get('/', 'ClassroomController@index')->name('index');
            Route::get('/{classroom}', 'ClassroomController@show')->name('show');
            Route::post('/', 'ClassroomController@store')->name('store');
            Route::put('/admin/{classroom}', 'ClassroomController@updateAdmin')->name('update.admin');
            Route::put('/{classroom}', 'ClassroomController@update')->name('update');
            Route::delete('/{classroom}', 'ClassroomController@destroy')->name('destroy');
        });

        Route::prefix('subjects')->name('subjects.')->group(function () {
            Route::get('/', 'SubjectController@index')->name('index');
            Route::get('/{subject}', 'SubjectController@show')->name('show');
            Route::post('/', 'SubjectController@store')->name('store');
            Route::put('/{subject}', 'SubjectController@update')->name('update');
            Route::delete('/{subject}', 'SubjectController@destroy')->name('destroy');
        });

        Route::prefix('chapters')->name('chapters.')->group(function () {
            Route::get('/subject/{subject}', 'ChapterController@index')->name('index');
            Route::get('/{chapter}', 'ChapterController@show')->name('show');
            Route::post('/{subject}', 'ChapterController@store')->name('store');
            Route::put('/order/{chapter}', 'ChapterController@updateOrder')->name('order.update');
            Route::put('/{chapter}', 'ChapterController@update')->name('update');
            Route::delete('/{chapter}', 'ChapterController@destroy')->name('destroy');
        });

        Route::prefix('sub-chapters')->name('sub-chapters.')->group(function () {
            Route::get('/chapter/{chapter}', 'SubChapterController@index')->name('index');
            Route::get('/{subChapter}', 'SubChapterController@show')->name('show');
            Route::post('/{chapter}', 'SubChapterController@store')->name('store');
            Route::put('/order/{subChapter}', 'SubChapterController@updateOrder')->name('order.update');
            Route::put('/{subChapter}', 'SubChapterController@update')->name('update');
            Route::delete('/{subChapter}', 'SubChapterController@destroy')->name('destroy');
        });

        Route::prefix('cards')->name('cards.')->group(function () {
            Route::put('/order/{card}', 'CardController@updateOrder')->name('order.update');
            Route::post('/update/{card}', 'CardController@update')->name('update');
            Route::get('/sub-chapter/{subChapter}', 'CardController@index')->name('index');
            Route::get('/{card}', 'CardController@show')->name('show');
            Route::post('/{subChapter}', 'CardController@store')->name('store');
            Route::delete('/{card}', 'CardController@destroy')->name('destroy');
        });

        Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    });
});


